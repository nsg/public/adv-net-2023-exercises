# Block 2: Network Programmability

## Introduction

Welcome to the first exercise of the network programmability block. This week, we will delve deeper into network programmability.

The objectives of these exercises are two-fold:
- Get acquainted with the provided virtual machine (VM) environment, development frameworks, and toolchains.
- Understand the challenges in the Internet by hands-on P4 programming, like programming traffic engineering and load-balancing.

## Exercise Planning

What we envisioned so far is as follows:
- The host TA of the week presents the exercise.
- General questions are discussed.
- Personal questions can be addressed to specific TAs.
- After the exercise session, the #Exercises channel (in [Matrix](https://matrix.to/#/#adv-net-23:studentchat.ethz.ch)) remains open for inquiries.

## Development Environment Setup

### Access your own VM

To make your life easier, we provide everyone with a VM where all tools and software are pre-installed.

To access your VM, you will use SSH. SSH is a UNIX-based command-line interface and protocol for securely getting access
to a remote computer. System administrators widely use it to control network devices
and servers remotely. An SSH client is available by default on any Linux and MAC installation
through the Terminal application. For Windows 10 users, there is SSH functionality available in the Command Prompt. For other Windows users, a good and free SSH client is [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/).
Once you have installed an SSH client, use the following command to connect yourself to your
VM:

```
ssh -p X p4@duvel.ethz.ch
```

Where X = 2000 + your student number for this lecture that we have sent you by email.
For instance, if you are Student-7, use the following command:

```
ssh -p 2007 p4@duvel.ethz.ch
```

If you cannot connect to your VM,
please report it immediately during the exercise session.

**Optional:**

⚠️ Important: Do not change your VM password.

⚠️ Important: Do not erase the public key in the `~/.ssh/authorized_keys` file.

If you want to simplify the access to your VM, you can use SSH key authentication, but **do not change your
password**. If you want to download an entire directory (e.g., the configs directory) from your
VM to the current directory of your own machine, you can use scp:

```
scp -r -P X p4@duvel.ethz.ch:~/path to the directory .
```

Where X = 2000 + your student number. On Windows, you can use WinSCP7 to do that. Note the
dot at the end of the command and the capitalized P.

### VM Contents

Your VM, based on Ubuntu 20.04.6 LTS, includes:

- Suite of P4 Tools ([p4lang](https://github.com/p4lang/), [p4utils](https://github.com/nsg-ethz/p4-utils), etc)
- [Wireshark](https://www.wireshark.org/)
- [Mininet](http://mininet.org/) network emulator

### Remote Development

We need to be able to open and edit remote files in our local code editor to have a smooth development cycle. This way, we can work with our code locally and execute it remotely without any friction. Below, we explain how to achieve this for Visual Studio Code as an example:

1) Download Visual Studio Code.
2) Access VS Code; in the left-side dock enter `Extensions` menu.
3) Install `Remote - SSH` extension.
4) In the pop-up prompt: enter your SSH credentials as you did for the VM access.
5) Your "Remote Directory" should appear in the Explorer.
6) [Optional] Go to Extensions menu again and install `P4 Language Extension` in your remote machine for highlights and syntax check.

For VS Code, you can find further information [here](https://code.visualstudio.com/docs/remote/ssh).
Many other text editors provide similar functionality. For other development environments, consult the relevant documentation for remote file editing.

If you are already familiar with remote development, feel free to continue with your favorite code editor/setup.

## P4 Programming Tools and Framework

### Mininet Environment

Mininet creates a realistic virtual network, running actual kernel, switch, and application code on a single machine.
Thanks to [P4-Utils](#p4-utils), our exercises will not require direct interaction with Mininet.
When we need to know specific Mininet CLI commands, we will introduce them in the exercise.

In short, Mininet creates hosts using Unix namespaces and interconnects them using virtual interfaces and BMv2 switches. Linux namespaces provide a network stack for all the processes running within a specific namespace. Thus, network virtualization tools such as Mininet uses Linux network namespaces to instantiate isolated nodes in a topology (hosts, switches, routers, etc.).

### BMv2 Switch

Behavioral Model version 2 (BMv2) is the second version of the reference P4 software switch. Please refer to our [documentation](https://github.com/nsg-ethz/p4-learning/wiki/BMv2-Simple-Switch) for the details.

### Control Plane

After we build and run the BMv2 software switch, we can use the Simple Switch CLI to configure the switch and populate match-action tables.

To learn more about the Control Plane and its functions, please refer [here](https://github.com/nsg-ethz/p4-learning/wiki/Control-Plane).

### Scapy

Scapy is a packet manipulation library written in Python. Please check [here](https://github.com/nsg-ethz/p4-learning/wiki/Scapy) for further information.

We will use Scapy for packet creation and capture at end hosts in our exercises.

### Debugging and Troubleshooting

Monitoring traffic can be a potent tool when debugging your P4 program.

To sniff the traffic that is going through an interface, we will use `tcpdump`. Please refer [here](https://github.com/nsg-ethz/p4-learning/wiki/Debugging-and-Troubleshooting) if you are interested in other similar tools or learning more about debugging and troubleshooting.

To capture binary-level traffic with `tcpdump` run:

```bash
sudo tcpdump -XX -i <interface_name>
```

### P4-Utils

P4-Utils is an extension to Mininet that makes P4 networks easier to build, run, and debug. Please refer to the [documentation](https://nsg-ethz.github.io/p4-utils/index.html) for the details.

### p4app.json

`p4app.json` describes the topology that we want to create with the help of Mininet and the P4-Utils package.

To create the topology described in `p4app.json`, you have to call `p4run`, which by default will check if the file `p4app.json` exists in the path:

   ```bash
   sudo p4run
   ```
This will call a Python script that parses the configuration file, creates a virtual network of hosts and p4 switches using Mininet, compiles the P4 program, and loads it in the switch.

After running `p4run` you will get the Mininet CLI prompt.

### P4 References

1. [P4 Spec](https://p4.org/specs/): P4 Language and Related Specifications. You will need P4-16 Spec.
2. [P4 Tutorials](https://github.com/p4lang/tutorials): Official P4 Tutorials
3. [P4-guide](https://github.com/jafingerhut/p4-guide): Repository that contains a lot of useful information, examples, and tests of best practices, tips, and tricks.

## Exercises

In this section, we provide links to the weekly exercises.

To get the exercises ready in your VM, clone this repository in the `p4` user home directory, as illustrated below:

```
cd /home/p4/
git clone https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises.git
```

Update the local repository to get new tasks and solutions.
Remember to pull this repository before every exercise session:

```
cd /home/p4/adv-net-2023-exercises
git pull
```

### Week 1: Introduction to P4 with L2 Switching

- [Layer 2 Switch](./01-L2_Switching)

### Week 2: Load Balancing: ECMP and Flowlet Switching

- [Load Balancing: ECMP & Flowlet Switching](./02-Load_Balancing)
