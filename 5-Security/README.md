# Block 5: Security

<!--toc:start-->
- [Block 5: Security](#block-5-security)
  - [Introduction](#introduction)
  - [Tools](#tools)
    - [Monitoring traffic with TShark](#monitoring-traffic-with-tshark)
    - [Blocking traffic with  Iptables](#blocking-traffic-with-iptables)
  - [The attacks](#the-attacks)
    - [Setup](#setup)
    - [Attack 1: What a cheap attempt!](#attack-1-what-a-cheap-attempt)
    - [Attack 2: I see where you're going to!](#attack-2-i-see-where-youre-going-to)
    - [Attack 3: You are serious, but so are we!](#attack-3-you-are-serious-but-so-are-we)
    - [Final thoughts](#final-thoughts)
<!--toc:end-->

You can find the solutions in [`solutions/Solution.md`](solutions/Solution.md).

## Introduction

Welcome to the exercise of the security block!

You are the operator of a small network, and you received complaints from a customer not being able to reach a web service at a remotely connected machine.
After a few tests, you quickly conclude that something is fishy: there must be a DDoS attack going on!

During this exercise, we will guide you through three attacks with increasing difficulty and ask you to set up proper defenses in the routers.
While the lecture discussed large-scale attacks and introduced sophisticated defenses, we have a closer look at simpler attacks and how to detect them with standard tools.

## Tools

To defend against DDoS attacks, you first need to know _where_ the attack is actually happening.
Only then you can start to _defend_ against the attack.

Therefore, we introduce two handy tools: one for monitoring and one for blocking traffic.

### Monitoring traffic with TShark
From the [manual](https://www.wireshark.org/docs/man-pages/tshark.html):
> TShark is a network protocol analyzer. It lets you capture packet data from a live network [... and] print a decoded form of those packets to the standard output [...].

TShark lets us analyze the packets traversing the routers.
This will be handy to analyze some specific features, such as the IP addresses of the endpoints, the ports, the protocol, the packet length, and the content of the packets.

> 💡 You may be familiar with Wireshark.
> TShark is similar but works in the terminal without a graphical user interface (GUI).
> It is better suited to quickly analyze packets sent through the routers.

We have preinstalled TShark on your VM.
You can run it by invoking the command `tshark`.

TShark has many optional flags (arguments) that you can use.
To solve the exercise, the following will be sufficient

| Flag                     | Explanation                                                                                    | Example                                         |
|--------------------------|------------------------------------------------------------------------------------------------|-------------------------------------------------|
| `-i <capture interface>` | Capture packets only for a given interface (use `ifconfig` to see the interfaces of a device). | `tshark -i port_S1`                             |
| `-c <number>`            | Capture only a specific number of packets.                                                     | `tshark -c 5`                                   |
| `-f <capture filter>`    | Capture only packets that match a specific filter (see below).                                 | `tshark -f host 1.0.0.2`                        |
| `-T fields`              | Display only selected fields (e.g., packet content, the source IP and the destination MAC)     | `tshark -T fields -e data -e ip.src -e eth.dst` |
| `-z <statistics>`        | Collect and display various types of statistics (see below).                                   | `tshark -z conv,ip`                             |

For the *capture filter*, there is a large variety of possibilities.
Again, we list the ones you need to solve the exercise and refer to the [manual](https://www.wireshark.org/docs/man-pages/pcap-filter.html) for a comprehensive list.

| Filter                  | Matches                                                 | Example                        |
|-------------------------|---------------------------------------------------------|--------------------------------|
| `<proto>`               | Packets of protocol type `<proto>`                      | `tshark -f tcp`                |
| `host <ip address>`     | Packets with `<ip address>` as source _or_ destination  | `tshark -f "host 1.0.0.2"`     |
| `src host <ip address>` | Packets with `<ip address>` as source                   | `tshark -f "src host 1.0.0.2"` |
| `port <number>`         | Packets with `<number>` as source _or_ destination port | `tshark -f "port 80"`          |

You can negate filters using `!`, as well as combine multiple ones using `&&` (concatenation) and  `||` (alternation).
For example, we can match all TCP packets towards the IP address `1.0.0.2` with `tshark -f "tcp && dst host 1.0.0.2"`.
Make sure to put the filter in quotes, otherwise you will see the warning
``` sh
tshark: A default capture filter was specified both with "-f" and with additional command-line arguments.
```

The *statistics* flag comes with many useful predefined collectors. Please see the [manual](https://www.wireshark.org/docs/man-pages/tshark.html) for a full list of available options.
The most useful collectors for this exercise are:

| Statistics  | Explanation                                                        | Example               |
|-------------|--------------------------------------------------------------------|-----------------------|
| `conv,eth`  | Create a table that lists all conversations between MAC addresses. | `tshark -z conv,eth`  |
| `conv,ip`   | Create a table that lists all conversations between IP addresses.  | `tshark -z conv,ip`   |
| `plen,tree` | Create a table that shows the distribution of the packet lengths.  | `tshark -z plen,tree` |

With this, you should have enough knowledge to find the source of the attack.
Let's see how to defend against them!

### Blocking traffic with  Iptables

Next, we give you a short introduction to `iptables` that we will use to block malicious packets.
We focus on the mechanics that are important for this exercise; for an in-depth introduction, we refer to the [Iptables tutorial](https://www.frozentux.net/iptables-tutorial/iptables-tutorial.html).

In a system with `iptables` enabled, every incoming packet will go through multiple stages before it is further processed by the application or the network.
The figure below gives you an intuition on how a packet is processed.
We will focus on the **filtering table (green)** which allows us to drop packets selectively.
Other stages deal with network address translation (nat; purple color), adjust the IP headers (mangle; blue color), and track connections (raw; rose).
![iptables Process Flow](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fen.vcenter.ir%2Fwp-content%2Fuploads%2F2017%2F05%2Fiptables-definition.jpg&f=1&nofb=1&ipt=5fb4451210ebcc330e0b3954af001d62629ca79149054f82b39dec23c968e1c9&ipo=images)

As we will block traffic within the routers in this exercise, we will add rules to the `FORWARD` filter:
Every rule consists of two parts: which packets to match and what to do with them.
Hence, a typical rule will look like this:

``` sh
iptables --insert FORWARD [MATCH] -j [ACTION]
```

You can match packets using the following flags:

| Flag                                           | Matches                        | Example for `[MATCH]`                             |
|------------------------------------------------|--------------------------------|---------------------------------------------------|
| `-p <proto>`                                   | the protocol                   | `-p tcp`                                          |
| `-s <ip addr>`                                 | the source IP address          | `-s 1.0.0.2`                                      |
| `-d <ip addr>`                                 | the destination IP address     | `-d 1.0.0.2`                                      |
| `--sport <number>`                             | the source port                | `-sport 22`                                       |
| `--dport <number>`                             | the destination port           | `-dport 22`                                       |
| `-m mac --mac-source <MAC ADDRESS>`            | the mac source address         | `-m mac --mac-source 01:23:45:67:89:ab`           |
| `-m length --length <from>:<to>`               | the packet length (in a range) | `-m length --length 10:20`                        |
| `-m string --algo bm --hex-string <hexstring>` | the content of the packet      | `-m string --algo bm --hex-string "\|DEADBEEF\|"` |

Possible actions include:

| `[ACTION]` | Description                                                           |
|------------|-----------------------------------------------------------------------|
| `ACCEPT`   | Accept the packet and process it further in the next stage            |
| `REJECT`   | Reject the packet and send an error back to the sender of the packet. |
| `DROP`     | Drop the package without informing the sender about it.               |

> 📝 When do you prefer `REJECT` over `DROP`? Which of the two actions is better suited to defend against attacks? Why?

You can manipulate the active Iptables rules with the following flags:

| Flag (short/long) | Explanation                                        | Example                                          |
|-------------------|----------------------------------------------------|--------------------------------------------------|
| `-A --append`     | Add a rule at the end.                             | `iptables -A FORWARD --protocol udp -j REJECT`   |
| `-D --delete`     | Remove specified rules.                            | `iptables -D FORWARD --protocol udp -j REJECT`   |
| `-F --flush`      | Remove all rules.                                  | `iptables -F`                                    |
| `-I --insert`     | Add a rule at a given position (e.g., position 2). | `iptables -I FORWARD 2 --protocol udp -j REJECT` |
| `-L --list`       | Show all rules                                     | `iptales -L`                                     |
| `-v --verbose`    | Show more information                              | `iptables -vL`                                   |

> 💡 A handy combination is `iptables -vL` which displays the number of matched packets for every rule.
> This lets you verify the effectiveness of your rules!

Iptables processes the rules in order:
After the first rule matching a packet, this action will immediately be applied, and no further rule will be checked.
If you thus want to insert before another one, use the command `iptables -L --line-numbers` to find out the desired line number.

## The attacks

### Setup

The topology for this exercise is as follows:

<p align="center">
<img src="images/network.png" title="Network Topology">
<p/>


The network consists of the following components:
* five hosts (`h1` to `h5`) with IP addresses as specified in the picture
* two switches (`s1`, `s2`) doing simple packet forwarding
* and three routers (`r1`, `r2`, `r3`) with interfaces as specified in the figure (i.e., `r1` has an interface `port_S1`  towards `s1`).
* several links as shown in the topology:
  * Links between routers (i.e., `(r1, r2)`, `(r1, r3)`) have a bandwidth of 2Mbps. These links are marked *red* in the above figure.
  * All other links (marked bold in the figure) have a bandwidth of 10Mbps.

The complaining customer controls `h1` and wants to upload **10 megabits of encrypted data** with TCP to the web service hosted at `h5`.
In the contract, you guaranteed the customer to access this service with a 1Mbps throughput, so your task is to restore this guarantee.

For this, you can use the monitoring and blocking tools discussed above.
As a network operator, you can only control the routers, but not the switches or the hosts!

We provide you the script `network.py` that spins up a mininet topology and simulates the traffic in the network.
We next describe the options you can pass to this script:

- To simulate the network without malicious traffic, run the following command:
  ``` sh
  sudo python network.py
  ```

- You can run the attack (number 1 in the example below) with the command
  ``` sh
  sudo python network.py -a 1
  ```

- To make your live easier, you also have the option `-n` to run the network only with malicious traffic but not with benign one:
  ``` sh
  sudo python network.py -a 1 -n
  ```

- If you have found the `iptables` commands to defend against an attack, enter them into the corresponding file in `skeleton/sol{1,2,3}.sh` and apply them:
  ``` sh
  sudo python network.py -a 1 -s 1
  ```

  Note that this allows you to apply the solution of previous attacks to new ones:
  ``` sh
  sudo python network.py -a 2 -s 1
  ```

You can terminate the simulation by hitting `Ctrl-C`.
Note that the benign client `h1` stops to send traffic after transmitting 10Mb.
However, the attacker will continue to send malicious traffic forever.

To execute a command (e.g., `ifconfig`, `tshark`, or `iptables`) in a router (e.g., `r1`), execute the following:
``` sh
mx r1 ifconfig
mx r1 tshark
mx r1 iptables
```

> ⚠ Be aware, that if you need to pass an option in quotes, use nested quotes:
> ``` sh
> mx r1 tshark -f '"host 1.0.0.2"'
> ```

To have multiple windows inside the terminal, we suggest you to use [tmux](https://github.com/tmux/tmux/wiki).
This lets you easily analyze what is going on in different routers while running the attack at the same time in another window.
We provide you the script `start_sessions.sh` that opens three windows which you can select with your mouse.
To exit tmux, close all windows by entering `exit` to each of them.
Please note that tmux is configured to use the prefix `Ctrl-a` instead of the default `Ctrl-b`.

Feel free to use another tool (or have multiple SSH sessions at the same time) if this suits you better.

> ⚠ _While it is technically possible to reverse-engineer the attacks by looking into our script (`network.py` or anything in `spoiler/`), this is definitely not the goal of this exercise.
> Stick to what you are supposed to do!_


### Attack 1: What a cheap attempt!

This attack is relatively simple, and the attacker does only one nasty thing.

1. Open three tmux sessions with the command
   ```sh
   ./start_sessions.sh
   ```
2. In the upmost window, run the attack scenario with
   ``` sh
   sudo python network.py -a 1
   ```
   You'll see a drastically reduce throughput rate.
3. In another window, analyze the traffic in the routers with
   ``` sh
   mx [ROUTER] tshark [OPTIONS]
   ```
4. Drop the malicious traffic in the routers with
   ``` sh
   mx [ROUTER] iptables [OPTIONS]
   ```
   Note that the network has some _latency_ due to buffered packets: the throughput may not immediately go back to the normal state, but take a few seconds to recover.
5. If you think you have found the correct Iptables rules, enter them in `skeleton/sol1.sh`, and restart the simulation with
   ``` sh
   <Ctrl-C>
   sudo python network.py -a 1 -s 1
   ```
   If the client again achieves a throughput to roughly 1Mbps, you successfully defend against the attack!

<details>
<summary>🪄 Hints (spoiler alert!)</summary>
You can defend against this attack from within a single router.

Try to find where the traffic is coming from and defend as close to the edge as possible!
</details>

📝 After you have installed the correct rules, answer the following questions:
1. How realistic is the attack scenario?
2. Would your defense be acceptable in the real world? Why or why not?
3. Do you see other (potentially better) ways to defend against this attack?

### Attack 2: I see where you're going to!

The adversary tries to evade our defense from above!
What can you do against it?

Follow the same steps before, but this time run the attack with
``` sh
sudo python network.py -a 2
```
and enter the solutions to `skeleton/sol2.sh`.

<details>
<summary>🪄 Hint 1 (spoiler alert!)</summary>
Not all traffic goes to `h5`.
</details>

<details>
<summary>🪄 Hint 2 (spoiler alert!)</summary>
Ensure that you do not disconnect `h1` from `h3`!
How can you do this?
<details>
<summary>🪄 Hint 2.1</summary>
It seems like `h1` is sending traffic to `h3`, but is this really the case?
Check if the adversary spoofs the  IP address.
</details>
<details>
<summary>🪄 Hint 2.2</summary>
Inspect the MAC addresses of the packets.

Can you see different source MAC addresses for packets with the same source IP address?
</details>

<details>
<summary>🪄 Hint 2.3</summary>
Block traffic based on the MAC address.
</details>
</details>


📝 After you have installed the correct rules, answer the following questions:
1. How realistic is the attack scenario?
2. Would your defense be acceptable in the real world? Why or why not?
3. Do you see other (potentially better) ways to defend against this attack?

### Attack 3: You are serious, but so are we!

Okay, the attack gets more serious:
Again, the attacker evades our defense from before.
But how did they do it?
And - more importantly - how can you defend against it?

Follow the same steps before, but this time run the attack with
``` sh
sudo python network.py -a 3
```
and enter the solutions to `skeleton/sol3.sh`.

<details>
<summary>🪄 Hints (spoiler alert!)</summary>
Some parts of the attack are similar to the previous ones.
Apply the same defenses and check what traffic reaches `h5`.

How can you differentiate the remaining malicious traffic from the benign one?
</details>

<details>
<summary>🪄 Hints 2 (spoiler alert!)</summary>
The attacker is in `h2` and spoofs both, the MAC address and the IP address (check the hints of the previous attack for how to observe that)!

But maybe you can differentiate the content of the malicious packets?

<details>
<summary>🪄 Hints 2.1: Content of the malicious packets</summary>
You can display the content of the packets using `tshark -T fields -e data`
</details>

<details>
<summary>🪄 Hints 2.2: Blocking based on the packet data</summary>
You can block all packets containing the hex string `0x77` using

```sh
iptables -A FORWARD -m string --algo bm --hex-string '"|77|"' -j DROP
```

</details>
</details>

📝  After you have installed the correct rules, answer the following questions:
1. How realistic is the attack scenario?
2. Would your defense be acceptable in the real world? Why or why not?
3. Do you see other (potentially better) ways to defend against this attack?

### Final thoughts

📝 There are multiple ways to defend against the above attacks; we want you to think about the following possibilities:
1. What scenarios could you "defend" against by adding new links between routers and cleverly rerouting the traffic?
2. Assume you could also control the switches `s1` and `s2`.
   - How does this help to defend against the attacks?
   - How can you ensure no host can spoof the MAC or IP address?
   - Would it be possible to defend solely in the switches?
   - _Optional_: Write down the pseudo-code to defend against the attacks.
     Share your code with us and your colleagues in the [Matrix chat room](https://matrix.to/#/#adv-net-23:studentchat.ethz.ch), so that we can discuss it.
