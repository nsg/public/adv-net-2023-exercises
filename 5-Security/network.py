from p4utils.mininetlib.network_API import NetworkAPI
import argparse
import os
import os.path
import time
import subprocess
import glob

from spoiler.python.utils.listener import listen
from spoiler.python.utils.utils import parse_skeleton

BASE_FOLDER = "spoiler"
ROUTERS_FOLDER = f"{BASE_FOLDER}/routers"
ATTACK_FOLDER = f"{BASE_FOLDER}/attacks"
PYTHON_FOLDER = f"{BASE_FOLDER}/python"
UTILS_FOLDER = f"{PYTHON_FOLDER}/utils"
CONTROLLER_FOLDER = f"{PYTHON_FOLDER}/switches"
P4SRC_FOLDER = f"{BASE_FOLDER}/p4src"

parser = argparse.ArgumentParser()
parser.add_argument(
    "-a",
    "--attack",
    type=int,
    help="The attack level to be executed. If ommited, then only benign traffic will be there.",
    choices=[1, 2, 3],
)
parser.add_argument(
    "-s",
    "--solution",
    type=str,
    help="""A path to the executable script that will be executed before the experiment.\n
    Use this to defend against the DDoS attacks.
    You can also pass a single number, in which case skeleton/sol[number].sh will be used""",
)
parser.add_argument(
    "-n",
    "--no_benign_traffic",
    help="If passed, there will be no benign, but only malicious traffic.",
    action="store_true",
)
parser.add_argument(
    "-v",
    "--verbose",
    help="Make the output more verbose.",
    action="store_true",
)
args = parser.parse_args()

net = NetworkAPI()


# Network general options
net.setLogLevel("info")
net.setCompiler(p4rt=True)
net.execScript(f"python {CONTROLLER_FOLDER}/controller.py", reboot=True)
net.execScript(
    f"python {CONTROLLER_FOLDER}/l2_learning_controller.py s2 digest &", reboot=True
)


if args.verbose:
    net.enablePcapDumpAll()
    net.enableLogAll()

# net.enableCli()
net.disableCli()
net.enableArpTables()

# Defense script
if args.solution is not None:
    solution = (
        f"skeleton/sol{args.solution}.sh" if args.solution.isdigit() else args.solution
    )

    if not solution.startswith("./"):
        solution = "./" + solution
    if not os.path.isfile(solution):
        print(f"[ERROR] solution {solution} does not exist.")
        exit(1)
    if not os.access(solution, os.X_OK):
        print(
            f"[ERROR] solution {solution} exists but is not executable. Hint: make it executable with the command 'chmod +x {solution}'"
        )
        exit(1)
    with open(solution, "r") as file:
        try:
            parse_skeleton(file.readlines())
        except ValueError as e:
            print(
                f"[ERROR] Could not apply solution {solution}: {e}"
            )
            exit(1)

    print(f"[INFO] Plan execution of solution {solution} after network boot")
    net.execScript(solution)

# Network definition

# Switches
net.addP4RuntimeSwitch("s1")
net.setP4Source("s1", f"./{P4SRC_FOLDER}/l2_basic_forwarding.p4")
net.addP4Switch("s2")
net.setP4Source("s2", f"./{P4SRC_FOLDER}/l2_learning_digest.p4")
# Hosts
net.addHost("h1")
net.setDefaultRoute("h1", "1.0.0.1")
net.enableLog("h1")  # We need this log to check the success of the attack.
net.addHost("h2")
net.setDefaultRoute("h2", "1.0.0.1")
net.addHost("h3")
net.setDefaultRoute("h3", "1.7.0.1")
net.addHost("h4")
net.setDefaultRoute("h4", "1.7.0.1")
net.addHost("h5")
net.setDefaultRoute("h5", "1.4.0.1")


# Routers
net.addRouter(
    "r1",
    conf_dir=f"./{ROUTERS_FOLDER}",
    int_conf=f"./{ROUTERS_FOLDER}/r1.conf",
    ldpd=True,
)
net.addRouter(
    "r2",
    conf_dir=f"./{ROUTERS_FOLDER}",
    int_conf=f"./{ROUTERS_FOLDER}/r2.conf",
    ldpd=True,
)
net.addRouter(
    "r3",
    conf_dir=f"./{ROUTERS_FOLDER}",
    int_conf=f"./{ROUTERS_FOLDER}/r3.conf",
    ldpd=True,
)

# Links
net.addLink("h1", "s1")
net.setIntfIp("h1", "s1", "1.0.0.2/24")
net.setIntfMac("h1", "s1", "BE:60:0D:BE:60:0D")

net.addLink("h2", "s1", intfName2="port_H2")
net.setIntfIp("h2", "s1", "1.0.0.3/24")
net.setIntfMac("h2", "s1", "BA:DC:0F:FE:ED:AD")

net.addLink("h3", "s2")
net.setIntfIp("h3", "s2", "1.7.0.2/24")

net.addLink("h4", "s2")
net.setIntfIp("h4", "s2", "1.7.0.3/24")

net.addLink("h5", "r3", intfName2="port_H5")
net.setIntfIp("h5", "r3", "1.4.0.2/24")

net.addLink("s1", "r1", intfName2="port_S1")
net.setIntfMac("r1", "s1", "60:0D:BE:EF:CA:FE")

net.addLink("s2", "r2", intfName2="port_S2")

# Set all connections involving switches to have bandwidth 10
net.setBwAll(10)

net.addLink("r1", "r2", intfName1="port_R2", intfName2="port_R1")
net.addLink("r2", "r3", intfName1="port_R3", intfName2="port_R2")

# But routers only have bandwidth 2 towards each other
net.setBw("r1", "r2", 2)
net.setBw("r2", "r3", 2)

# Nodes general options
if not args.no_benign_traffic:
    net.addTaskFile(f"{BASE_FOLDER}/benign_traffic.txt")
if args.attack is not None:
    net.addTaskFile(f"{ATTACK_FOLDER}/attack{args.attack}.txt")

# Start the network
net.startNetwork()


try:
    if not args.no_benign_traffic:
        listen("log/h1_scheduler.log", verbose=args.verbose)
    else:
        while True:
            time.sleep(1)
except KeyboardInterrupt:
    net.stopNetwork()

    # Remove the compilers outputs
    for compiler in net.compilers:
        compiler.clean()
    for f in glob.glob(f"{P4SRC_FOLDER}/*.p4i"):
        print(f"Remove {f}")
        os.remove(f)

    # Exit all the mx students might have
    subprocess.call(["pkill", "mx"])
