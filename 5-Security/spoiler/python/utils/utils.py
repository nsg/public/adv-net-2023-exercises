import subprocess
import math
import sys
import os
import json
import re


def load_conf(conf_file):
    with open(conf_file, "r") as f:
        config = json.load(f)
    return config


def log_error(*items):
    print(*items, file=sys.stderr)


def run_command(command):
    print(command)
    return os.WEXITSTATUS(os.system(command))


def get_user():
    """Try to find the user who called sudo/pkexec."""
    user = subprocess.check_output("echo ${SUDO_USER:-${USER}}", shell=True)
    return user.strip().decode("utf-8")


# Rate Conversions
# =================


def _parse_rate(rate):
    """Parse a given rate in B/s.

    Args:
        rate (str or float): Rate.

    Note:
        String input size can be given with the following magnitudes:
        **bps**, **Kbps**, **Mbps** and **Gbps**. If the input rate is a
        :py:class:`int`, then it is assumed as bps.

    Returns:
        float: rate in B/s.
    """
    conversions = {"bps": 1, "Kbps": 1e3, "Mbps": 1e6, "Gbps": 1e9}

    if isinstance(rate, int):
        return rate / 8
    elif isinstance(rate, str):
        regex = r"^(?P<rate>\d+(?:\.\d+)?)\s*(?P<unit>\w+)$"
        match = re.search(regex, rate)
        if match is not None:
            rate = match.group("rate")
            unit = match.group("unit")
            if unit in conversions.keys():
                return float(rate) * conversions[unit] / 8
            else:
                Exception('unit "{}" not recognized in "{}"!'.format(unit, rate))
        else:
            raise Exception('cannot parse "{}"!'.format(rate))
    else:
        raise Exception("conversion from {} not supported!".format(type(rate)))


def _parse_size(size):
    """Parse a given size in Bytes.

    Args:
        size (str or int): size.

    Note:
        String input size can be given with the following magnitudes:
        **B**, **KB**, **MB** and **GB**. If the input rate is a
        :py:class:`int`, then it is assumed as bps.

    Returns:
        int: size in Bytes.
    """

    conversions = {"B": 1, "KB": 1e3, "MB": 1e6, "GB": 1e9}

    if isinstance(size, int):
        return size
    elif isinstance(size, str):
        regex = r"^(?P<size>\d+(?:\.\d+)?)\s*(?P<unit>\w+)$"
        match = re.search(regex, size)
        if match is not None:
            size = match.group("size")
            unit = match.group("unit")
            if unit in conversions.keys():
                return math.ceil(float(size) * conversions[unit])
            else:
                Exception('unit "{}" not recognized in "{}"!'.format(unit, size))
        else:
            raise Exception('cannot parse "{}"!'.format(size))
    else:
        raise Exception("conversion from {} not supported!".format(type(size)))


def setSizeToInt(size):
    """ " Converts the sizes string notation to the corresponding integer
    (in bytes).  Input size can be given with the following
    magnitudes: B, K, M and G.
    """
    if isinstance(size, int):
        return size
    elif isinstance(size, float):
        return int(size)
    try:
        conversions = {"B": 1, "K": 1e3, "M": 1e6, "G": 1e9}
        conversions.update({"B": 1, "KB": 1e3, "MB": 1e6, "GB": 1e9})

        digits_list = "0123456789."
        digit = float("".join([x for x in size if x in digits_list]))
        magnitude = "".join([x for x in size if x not in digits_list])
        magnitude = magnitude.upper()
        magnitude = conversions[magnitude]
        return int(magnitude * digit)
    except:
        print("Conversion Fail")
        return 0


def setRateToInt(size):
    """ " Converts the sizes string notation to the corresponding integer
    (in bytes).  Input size can be given with the following
    magnitudes: Bbps, Kbps, Mbps and Gbps.
    """
    try:
        conversions = {"bps": 1, "kbps": 1e3, "mbps": 1e6, "gbps": 1e9}

        digits_list = "0123456789."
        digit = float("".join([x for x in size if x in digits_list]))
        magnitude = "".join([x for x in size if x not in digits_list])
        magnitude = magnitude.lower()
        magnitude = conversions[magnitude]
        return int(magnitude * digit)
    except:
        print("Conversion Fail")
        import traceback

        traceback.print_exc()
        return 0


def parse_skeleton(lines: [str]):
    """Parse whether a the content of a skeleton is valid.

    Args:
        lines ([str]): The content of the skeleton.

    Note:
        This function checks, whether the contant contains a shebang (#!),
        and whether every non-empty, non-comment line begins with `mx r`

    Raises an error if not all checks pass.
    """
    if not lines[0].startswith("#!/bin/sh"):
        raise ValueError("The skeleton file does begin with the '#!/bin/sh'")
    for line in lines[1:]:
        if line.isspace() or line == "":
            # The line is empty
            continue
        if re.match(r'^\s*\#', line):
            # The line is a comment
            continue
        if not re.match(r'^\s*mx r[123] ', line):
            # The line is a comment
            raise ValueError(" The contains a non-comment line that does not start with mx r[123]'.")
