#!/usr/bin/env python3
from random import randrange, choice

import csv
import os
import time
import math
import socket
import struct
from spoiler.python.utils.traffic import *
from spoiler.python.utils.utils import _parse_rate, _parse_size

from scapy.all import IP, UDP, Raw, conf, TCP, Ether


TCP_SEQ_NUM_SIZE = 2**32


def get_bytes(n, randomized):
    print(f"randomized: {randomized}")
    if randomized:
        return os.urandom(n)
    else:
        return bytes(n)


def send_spoofed_flow(
    src,
    dst="127.0.0.1",
    sport=5000,
    dport=5001,
    proto="udp",
    tos=0,
    rate="10 Mbps",
    duration=10,
    iface="eth0",
    payload_size=None,
    random_payload=True,
    max_burst_size=UDP_MAX_BURST_SIZE,
    out_csv=None,
    **kwargs,
):
    """Sending function that lets you spoof the IP address and keeps a constant rate and logs sent packets to a file.
    Args:
        src (str): Source IPv4 address
        dst (str, optional): Destination IP. Defaults to '127.0.0.1'.
        sport (int, optional): Source port. Defaults to 5000.
        dport (int, optional): Destination port. Defaults to 5001.
        proto (str, optional): Protocol to use. Defaults to 'udp'
        tos (int, optional): Type of Service. Defaults to 0.
        rate (float or str, optional): Flow rate. Defaults to '10 Mbps'.
        duration (float, optional): Flow duration in seconds. Defaults to 10.
        payload_size (int, optional): UDP payload in bytes. Defaults to UDP_MAX_PAYLOAD.
        random_payload (bool, optional): Whether or not to randomize the payload. If false, only zeroes will be sent. Defaults to True.
        max_burst_size (int, optional): UDP burst size in number of packets. Defaults to UDP_MAX_BURST_SIZE.
        out_csv (str, optional): Log of sent packets with timestamps. Defaults to None in which case nothing will be written.

    Note:
        ``max_burst_size`` cannot be smaller than ``1``.
    """

    sport = int(sport)
    dport = int(dport)
    duration = float(duration)

    if random_payload == "False":
        random_payload = False
    if random_payload == "True":
        random_payload = True
    assert isinstance(random_payload, bool)

    if payload_size is None:
        payload_size = UDP_MAX_PAYLOAD if proto == "udp" else TCP_MAX_PAYLOAD

    # Convert rates to B/s
    rate = _parse_rate(rate)
    # print("rate", rate, "\n")

    # Sanity checks
    assert isinstance(dst, str)  # Desination IP must be a string
    assert rate > 0  # The flow must have a positive rate
    assert (
        isinstance(sport, int) and sport > 0 and sport < 2**16
    )  # Check valid port number
    assert (
        isinstance(dport, int) and dport > 0 and dport < 2**16
    )  # Check valid port number
    assert isinstance(tos, int) and tos >= 0 and tos < 2**8  # Check valid ToS value
    assert (
        isinstance(duration, float) or isinstance(duration, int)
    ) and duration >= 0  # Duration must be positive
    assert (
        isinstance(payload_size, int)
        and payload_size > 13
        and (
            (proto == "udp" and payload_size <= UDP_MAX_PAYLOAD)
            or (proto == "tcp" and payload_size <= TCP_MAX_PAYLOAD)
        )
    )  # Check valid payload size
    assert (
        isinstance(max_burst_size, int) and max_burst_size > 0
    )  # The maximum burst size must be at least 1 packet

    assert proto == "udp" or proto == "tcp"

    # Open .csv file
    if out_csv is not None:
        output = open(out_csv, "w", newline="")
        # Fields of the .csv
        fields = ["seq_num", "t_timestamp"]
        # CSV writer
        csv_writer = csv.DictWriter(output, fieldnames=fields)
        # Write header
        csv_writer.writeheader()

    # Open socket
    #
    s = conf.L3socket(iface=conf.iface)

    # Initialize token bucket
    token_bucket = 0
    # Initialize burst counter
    brst_count = 0
    # Initialize sequence number
    seq_num = 1

    # Save start time
    startTime = lastTime = time.time()

    # Compute end time
    if duration > 0:
        endTime = startTime + duration
    else:
        endTime = None

    if proto == "udp":
        payload = (
            IP(src=src, dst=dst)
            / UDP(sport=sport, dport=dport)
            / Raw(get_bytes(payload_size - 13), random_payload)
        )
    else:  # proto == tcp
        syn_packet = (
            IP(src=src, dst=dst)
            / TCP(sport=sport, dport=dport, flags="S")
            / Raw(get_bytes(payload_size, random_payload))
        )
        s.send(syn_packet)

        payload = (
            IP(src=src, dst=dst)
            / TCP(sport=sport, dport=dport)
            / Raw(get_bytes(payload_size, random_payload))
        )

    while True:
        while token_bucket >= payload_size and brst_count < max_burst_size:
            # Get timestamp
            timestamp = time.time()

            # Send directly over socket (sendp would be much slower!)
            s.send(payload)

            if out_csv is not None:
                # Save log to the .csv file
                csv_writer.writerow({"seq_num": seq_num, "t_timestamp": timestamp})

            # Increase the sequence number
            seq_num += 1
            # Increse the burst counter
            brst_count += 1
            # Remove tokens from the bucket
            token_bucket -= payload_size + ETHERNET_HEADER + IPV4_HEADER + UDP_HEADER
            # If the sequence number needs more bytes, raise exception
            if math.ceil(seq_num.bit_length() / 8) > payload_size - 4:
                raise Exception("cannot store sequence number in packet payload!")

        # Get current time
        currentTime = time.time()

        # Break if duration expired
        if endTime is not None:
            if currentTime >= endTime:
                break

        # Compute elapsed time
        diffTime = currentTime - lastTime
        lastTime = currentTime

        # Add tokens to bucket
        token_bucket += rate * diffTime
        # Reset the burst counter
        brst_count = 0

        # Sleep for at least one packet in the token_bucket
        time.sleep(max(payload_size - token_bucket, 0) / rate)

    # Close socket
    s.close()
    if out_csv is not None:
        # Close .csv file
        output.close()


def attack_random_flows(
    sources,
    victim_src,
    victim_src_mac=None,
    dst_mac=None,
    dst="127.0.0.1",
    sport=5000,
    dport=5001,
    rate="10 Mbps",
    duration=10,
    iface="eth0",
    random_payload=True,
    max_burst_size=UDP_MAX_BURST_SIZE,
    out_csv=None,
    **kwargs,
):
    """Sending function that randomizes the source ip address and the ports
    Args:
        sources (str): comma_seperated source IPv4 addresses (without spaces!)
        victim_src (str): Source IPv4 address of the victim
        dst (str, optional): Destination IP. Defaults to '127.0.0.1'.
        sport (int, optional): Source port. Defaults to 5000.
        dport (int, optional): Destination port. Defaults to 5001.
        rate (float or str, optional): Flow rate. Defaults to '10 Mbps'.
        duration (float, optional): Flow duration in seconds. Defaults to 10.
        random_payload (bool, optional): Whether or not to randomize the payload. If false, only zeroes will be sent. Defaults to True.
        max_burst_size (int, optional): UDP burst size in number of packets. Defaults to UDP_MAX_BURST_SIZE.
        out_csv (str, optional): Log of sent packets with timestamps. Defaults to None in which case nothing will be written.

    Note:
        ``max_burst_size`` cannot be smaller than ``1``.
    """

    print(f"random_payload: {random_payload}")

    sport = int(sport)
    dport = int(dport)
    duration = float(duration)

    if random_payload == "False":
        random_payload = False
    if random_payload == "True":
        random_payload = True
    assert isinstance(random_payload, bool)

    # Convert rates to B/s
    rate = _parse_rate(rate)
    # print("rate", rate, "\n")

    # Sanity checks
    assert isinstance(sources, str)  # Sources must be a string
    assert " " not in sources
    assert isinstance(victim_src, str)  # Victim source IP must be a string
    assert isinstance(dst, str)  # Desination IP must be a string
    assert rate > 0  # The flow must have a positive rate
    assert (
        isinstance(sport, int) and sport > 0 and sport < 2**16
    )  # Check valid port number
    assert (
        isinstance(dport, int) and dport > 0 and dport < 2**16
    )  # Check valid port number
    assert (
        isinstance(duration, float) or isinstance(duration, int)
    ) and duration >= 0  # Duration must be positive
    assert (
        isinstance(max_burst_size, int) and max_burst_size > 0
    )  # The maximum burst size must be at least 1 packet

    # If we have a victim_src_mac, then we also need the exact destination.
    assert victim_src_mac is None or dst_mac is not None

    # sources are a comma separated list
    sources = sources.split(",")
    assert "" not in sources

    if out_csv is not None:
        # Open .csv file
        output = open(out_csv, "w", newline="")
        # Fields of the .csv
        fields = ["seq_num", "t_timestamp"]
        # CSV writer
        csv_writer = csv.DictWriter(output, fieldnames=fields)
        # Write header
        csv_writer.writeheader()

    # Open socket
    s = conf.L2socket(iface=iface)

    # Initialize token bucket
    token_bucket = 0
    # Initialize burst counter
    brst_count = 0
    # Initialize sequence number
    seq_num = 1

    # Save start time
    startTime = lastTime = time.time()

    # Compute end time
    if duration > 0:
        endTime = startTime + duration
    else:
        endTime = None

    payload_size = min(UDP_MAX_PAYLOAD, TCP_MAX_PAYLOAD)
    while True:
        while token_bucket >= payload_size and brst_count < max_burst_size:
            # Get timestamp
            timestamp = time.time()

            src = victim_src if randrange(2) == 0 else choice(sources)

            proto = choice(["udp", "tcp"])

            ip_packet = Ether(src=victim_src_mac, dst=dst_mac) / IP(src=src, dst=dst)

            # Prepare payload
            if proto == "udp":
                payload_size = UDP_MAX_PAYLOAD
                packet = (
                    ip_packet
                    / UDP(sport=sport, dport=dport)
                    / Raw(get_bytes(payload_size, random_payload))
                )

            else:  # proto == tcp
                payload_size = TCP_MAX_PAYLOAD
                syn_packet = (
                    ip_packet
                    / TCP(sport=sport, dport=dport, flags="S")
                    / Raw(get_bytes(payload_size, random_payload))
                )
                try:
                    s.send(syn_packet)
                except OSError:
                    pass  # This can happen during shutdown process

                # Remove tokens from the bucket
                token_bucket -= len(syn_packet)

                packet = (
                    ip_packet
                    / TCP(sport=sport, dport=dport)
                    / Raw(get_bytes(payload_size, random_payload))
                )

            # Send directly over socket (sendp would be much slower!)
            try:
                s.send(packet)
            except OSError:
                pass  # This can happen during shutdown process

            if out_csv is not None:
                # Save log to the .csv file
                csv_writer.writerow({"seq_num": seq_num, "t_timestamp": timestamp})
            # Increase the sequence number
            seq_num += 1
            # Increse the burst counter
            brst_count += 1
            # Remove tokens from the bucket
            token_bucket -= len(packet)
            # If the sequence number needs more bytes, raise exception
            if math.ceil(seq_num.bit_length() / 8) > payload_size - 4:
                raise Exception("cannot store sequence number in packet payload!")

        # Get current time
        currentTime = time.time()

        # Break if duration expired
        if endTime is not None:
            if currentTime >= endTime:
                break

        # Compute elapsed time
        diffTime = currentTime - lastTime
        lastTime = currentTime

        # Add tokens to bucket
        token_bucket += rate * diffTime
        # Reset the burst counter
        brst_count = 0

        # Sleep for at least one packet in the token_bucket
        time.sleep(max(payload_size - token_bucket, 0) / rate)

    # Close socket
    s.close()

    if out_csv is not None:
        # Close .csv file
        output.close()
