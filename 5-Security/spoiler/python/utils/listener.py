import time
from spoiler.python.utils.traffic import parse_label


def listen(logfile, timeout=None, verbose=False):
    t_start = t_now = time.time()
    t_nothing_default = t_nothing = 2
    if timeout is not None:
        t_end = t_start + timeout
    f = open(logfile, "r+")
    prev_line = ""
    t_last = None
    while timeout is None or t_now < t_end:
        line = f.readline()
        if line != prev_line:
            (t, message) = parse_label(line)
            if message is not None:
                print(message, end="")
            elif verbose:
                print(line)
            if t is not None:
                t_nothing = t_nothing_default
                if "Finished" in message:
                    t_last = float("inf")  # we are done
                elif t_last is None or t_last < float("inf"):
                    t_last = float(t)
            time.sleep(0.01)
        else:
            time.sleep(0.5)

        # print(line)
        prev_line = line
        t_now = time.time()

        # print(t_last)
        # print(t_now)

        if t_last is not None and t_last + t_nothing < t_now:
            print(
                f"No data transmitted during the last {sum([i for i in range(t_nothing+1)])} seconds. What is going on??"
            )
            t_nothing += 1
            t_last = time.time()  # reset
