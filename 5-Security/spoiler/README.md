# ⚠ THIS FOLDER IS NOT FOR YOU (*if you are a student*)!

---

Otherwise, here is the structure of this folder:

## In the main folder (`../`)

- `README.md`: The task description for the students.
- `images`: Images for the README.
- `network.py`: Entry point for the students with options to launch different attacks and defense scripts.
- `start_sessions.sh`: A handy shortcut script to start a tmux session.
- `skeleton`: The entry point for students to enter their solution.
- `spoiler`: All configurations and scripts that students should not have access to.

## In this folder
The folder `spoiler` contains the following parts:

- `README.md`: This file.
- `benign_traffic.txt`: Schedule the task for sending the benign traffic from `h1` to `h5`.
- `attacks/`:
  - `attack1.txt`: Malicious traffic from `h3` to `h5`.
  - `attack2.txt`: Malicious traffic from `h2` to `h3`, IP spoofing
  - `attack3.txt`: Multiple malicious traffic, IP spoofing + MAC spoofing
- `solutions`: The solution for every attack
  - `sol1.sh`: iptable rules for attack 1
  - `sol2.sh`: iptable rules for attack 2
  - `sol3.sh`: iptable rules for attack 3
  - `Solution.md`: An in-depth explanation on how to find the iptable rules. Answer to the exercise questions.
- `p4src/`: Code for the switches
  - `l2_basic_forwarding.p4`: Static forwarding for `s1`
  - `l2_learning_digest.p4`: Dynamic forwarding (learning) for `s2`
- `python/`: 
  - `__init__.py`
  - `switches/`: Controller for switches
    - `controller.py`: Static forwarding for `s1`
    - `l2_learning_controller.py`: Dynamic forwarding (learning for `s2`)
  - `utils/`:
    - `listener.py`: Called from `network.py`; print out the transmission rate read from `log/h1_scheduler.log`
    - `spoofed_traffic.py`: Utility to send spoofed TCP and UDP traffic.
    - `traffic.py`: Utility to send/receive benign TCP and UDP traffic
    - `utils.py`: Utilities for converting rates etc.
- `routers/`: Configurations for the routers
  - `r1.conf`: Static forwarding to `s1` and `r2`
  - `r2.conf`: Static forwarding to `s2` and `r3`
  - `r3.conf`: Static forwarding to `h5` and `r2`
