# Solutions

Here, we describe the solutions to the DDoS exercises.
While the required commands to stop the attacks are listed in `sol{1,2,3}.sh`, we herein describe how to inspect what is going on step by step.

## Attack 1

The victim in `h1` cannot (or only poorly) reach the destination host `h5`; we see this by the decreased transmission rate in the console ("Sent 0.065101 MB after 1.02s with an average transmission rate of  0.06 Mbps").

### Finding the source of the attack

We first inspect whether `h1` is actually able to send _some_ traffic.
For this, we look at the flows at the closest router `r1` with the command `mx r1 tshark -i port_S1 -z conv,ip`:
```
...
================================================================================
IPv4 Conversations
Filter:<No Filter>
                    |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                    | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
1.0.0.2 <-> 1.4.0.2    96      6336     103    124870     199    131206     0.000000000         3.5162
================================================================================
```

We see that there is traffic _only_ from `h1` (`1.0.0.2`) to `h5` (`1.4.0.2`), so this does not allow us to identify the attacker.

Next, we look into the traffic that reaches `h5` at the router closest to `h5`, i.e. `r3`.
For that we use the command `mx r3 tshark -i port_H5 -z conv,ip`:
```
...
  521 2.889091491      1.7.0.2 → 1.4.0.2      UDP 1429 6491 → 6490 Len=1387
  522 2.894215096      1.7.0.2 → 1.4.0.2      UDP 1429 6491 → 6490 Len=1387
  523 2.899809065      1.7.0.2 → 1.4.0.2      UDP 1429 6491 → 6490 Len=1387
  524 2.905407385      1.7.0.2 → 1.4.0.2      UDP 1429 6491 → 6490 Len=1387
  525 2.910978684      1.7.0.2 → 1.4.0.2      UDP 1429 6491 → 6490 Len=1387
  526 2.916530913      1.7.0.2 → 1.4.0.2      UDP 1429 6491 → 6490 Len=1387
526 packets captured
================================================================================
IPv4 Conversations
Filter:<No Filter>
                    |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                    | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
1.4.0.2 <-> 1.7.0.2   616    880264       4      2360     620    882624     0.000000000         4.0665
1.0.0.2 <-> 1.4.0.2    46      3036      91    137774     137    140810     0.005712518         4.0548
================================================================================
```

We see that `h1` (`1.0.0.2`) can send a few packets (roughly 34 KB/s in the above output), but there is another traffic sending data at a much bigger rate (roughly 220KB/s).
We found the attacker!
It must be the host located with the IP address `1.7.0.2`, i.e., the host `h3`!
Observe from the captured packets that all the attack traffic is over `UDP`.

To confirm that, we check the traffic at the router closest to `h3`, i.e., `r2`.
We run the command `mx r2 tshark -i port_S2 -z conv,ip` to see
```
...
================================================================================
IPv4 Conversations
Filter:<No Filter>
                    |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                    | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
1.4.0.2 <-> 1.7.0.2    543    775947       3      1770     546    777717     0.000000000         3.0284
================================================================================

```

Indeed, the traffic is coming from `S2`, so all we need to do is to block the attack traffic.

### Defending

We block the attack traffic as close to the edge as possible.
In this case, we can drop the attack traffic at router `r2`.

To block exactly (and nothing more than) the observed attack traffic, we can install the corresponding iptables rule:
```
mx r2 iptables -A FORWARD -p udp --src 1.7.0.2 --dst 1.4.0.2 -j DROP
```

We see that the traffic from `h1` quickly recovers to a transmission rate of 1 Mbps.
After installing the firewall rule, you may observe transmission rates higher than 1 Mbps.
This is due to the buffered packets that already left `h1` and which can now finally be transmitted.


### Questions
1. **How realistic is the attack scenario?**
   - Having one host sending a lot of traffic is not too hard and can be considered realistic.
   - Not realistic are the small network size and the fact that there is precisely one known, benign customer (`h1`).
     In more realistic scenarios, much more people would be affected.
     This can make it in turn harder to defend: If there are gazillions of legitimate flows, it is harder to find the (few) malicious ones.

2. **Would your defense be acceptable in the real world? Why or why not?**

   Yes, disconnecting two specific hosts (`h3` and `h5`) does not cause too much harm.
   We thus consider it as acceptable.
   Moreover, `h3` might contact the ISP to clarify the situation.

3. **Do you see other (potentially better) ways to defend against this attack?**

   Alternative approaches include:
   - Drop all packets from `1.7.0.2`, independent of the destination IP address.
     While this might stop `h3` from attacking other services as well, we might block `h3` from legitimately connecting to other services.
   - Drop all packets except the ones having `1.0.0.2` or `1.4.0.2` as a source address.
     While this might work for this attack, the risk of blocking other benign traffic is generally too big.
   - Dropping all UDP traffic.
     While this might work for this attack, the number of false-positively dropped packets is too big for real-world application.
   - Reroute the traffic from `1.0.0.1/24` from `r1` directly to `r3` instead of going over `r2`.
     While there is no congested link anymore, `h5` could be DoSed at the application level.

## Attack 2

The victim in `h1` cannot (or only poorly) reach the destination host `h5`.
We see this with the error messages in the main console ("No data transmitted during the last 27 seconds. What is going on??").

### Finding the source of the attack

As before, we first inspect whether `h1` is actually able to send _some_ traffic.
For this, we look at the flows at the closest router `r1` with the command `mx r1 tshark -i port_S1 -z conv,ip`

```
...
================================================================================
IPv4 Conversations
Filter:<No Filter>
                    |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                    | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
1.0.0.2 <-> 1.7.0.2      0         0    5173   7764673    5173   7764673     1.313850238         7.8258
1.0.0.2 <-> 1.4.0.2      0         0       3       222       3       222     4.135473410         3.0333
================================================================================
```

We see that there is traffic from `1.0.0.2` to `h5` (`1.4.0.2`), but there is also traffic from `1.0.0.2` to `1.7.0.2`!
If we look at `r3`, we see that almost no traffic reaches `h5` (`mx r3 tshark -i port_H5 -z conv,ip`):
```
================================================================================
IPv4 Conversations
Filter:<No Filter>
                    |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                    | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
1.0.0.2 <-> 1.4.0.2      0         0       1        54       1        54     0.819466338         0.0000
================================================================================
```

By quickly checking the traffic from `r1` towards `r3` (`mx r1 tshark -i port_R3 -z conv,ip`) and from `r1` towards `r2` (`mx r1 tshark -i port_R2 -z conv,ip`), we see that all traffic is routed through `r2`, the link `r1--r3` is not used!
We can therefore deduce that the link `r1--r2` is congested, and thus causing the packets towards `1.4.0.2` to be dropped.

So is the customer really sending a large flow to `h3` and thus effectively blocking themselves from sending traffic to `1.4.0.2`?
Let's find this out by checking if traffic really originates from the same computer.

We do this by looking at the MAC addresses of the packets from `s1` to `r1`.
First, we look at the packets towards `1.4.0.2` with the command `mx r1 tshark -z conv,eth dst host 1.4.0.2`:

```
    1 0.000000000      1.0.0.2 → 1.4.0.2      TCP 74 6491 → 6490 [SYN] Seq=0 Win=65535 Len=0 MSS=1460 SACK_PERM=1 TSval=1674367830 TSecr=0 WS=8
    2 1.022976583      1.0.0.2 → 1.4.0.2      TCP 74 [TCP Retransmission] 6491 → 6490 [SYN] Seq=0 Win=65535 Len=0 MSS=1460 SACK_PERM=1 TSval=1674368853 TSecr=0 WS=8

2 packets captured
================================================================================
Ethernet Conversations
Filter:<No Filter>
                                               |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                                               | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
60:0d:be:ef:ca:fe    <-> be:60:0d:be:60:0d          2       148       0         0       2       148     0.000000000         1.0230
================================================================================
```

Looking at the direction of the traffic, we observe that the MAC address of the source of this flow is `be:60:0d:be:60:0d`, while the one of the next hop `r1` is `60:0d:be:ef:ca:fe` (quickly confirm that this is the mac address of `r1` with `mx r1 ifconfig`!).

Next we look at the packets towards `1.7.0.2` with the command `mx r1 tshark -z conv,eth dst host 1.7.0.2`:

```
...
1626 2.449466108      1.0.0.2 → 1.7.0.2      TCP 1514 [TCP Retransmission] 6491 → 6490 [SYN] Seq=0 Win=8192 Len=1460
1627 2.451082641      1.0.0.2 → 1.7.0.2      TCP 1514 [TCP Retransmission] 6491 → 6490 [SYN] Seq=0 Win=8192 Len=1460
1627 packets captured
================================================================================
Ethernet Conversations
Filter:<No Filter>
                                               |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                                               | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
60:0d:be:ef:ca:fe    <-> ba:dc:0f:fe:ed:ad       1620   2452680       0         0    1620   2452680     0.000000000         2.4511
60:0d:be:ef:ca:fe    <-> be:60:0d:be:60:0d          7       378       0         0       7       378     0.211201246         2.0416
================================================================================
```

Looking at the direction of the traffic, we observe that the MAC address of the source of this flow is `ba:dc:0f:fe:ed:ad`, while we again see the MAC address of `r1` (`60:0d:be:ef:ca:fe`).

Hence, we know that the attacker must be the host with MAC address `ba:dc:0f:fe:ed:ad`!

### Defending

We block the attack traffic as close to the edge as possible.
In this case, we can drop the attack traffic at router `r1`.

To block exactly (and nothing more than) the observed attack traffic, we can install the corresponding iptables rule:
```
mx r1 iptables -A FORWARD -m mac --mac-source ba:dc:0f:fe:ed:ad --dst 1.7.0.2 -j DROP
```

We see that the traffic from `h1` quickly recovers to a transmission rate of 1 Mbps.
After installing the firewall rule, you may observe transmission rates higher than 1 Mbps.
This is due to the buffered packets that already left `h1` and which can now finally be transmitted.


### Questions
1. **How realistic is the attack scenario?**

   _Same as for attack 1._

2. **Would your defense be acceptable in the real world? Why or why not?**

   _Same as for attack 1._

3. **Do you see other (potentially better) ways to defend against this attack?**

   Alternative approaches include:
   - Drop all packets towards `1.7.0.2`, independent of the source MAC address.
     While this might stop `h2` from attacking other services as well, this effectively blocks the connection to `1.7.0.2` for _anyone_!
     Hence, an attacker could launch a DoS against `h3` using exactly the same technique as for this exercise.
     This is thus not an acceptable solution!
   - Reroute the traffic from `1.0.0.1/24` from `r1` directly to `r3` instead of going over `r2`.
     There is no congested link anymore, and `h5` would not receive any other traffic than the benign one.
     Consequently, this defense would be effective and is acceptable.

## Attack 3

The victim in `h1` cannot (or only poorly) reach the destination host `h5`.
We see this with the error messages in the main console ("No data transmitted during the last 27 seconds. What is going on??").

This attack is more complex than the previous ones, as it is difficult to first identify all attack traffic and then block it entirely.
We therefore opt for an iterative approach in which we identify some attack traffic, block it, and then iterate again if this is not sufficient.

### Defending against the simplest portion of the attack traffic

As before, we first inspect what traffic is reaching `h5`.
We do this with `mx r3 tshark -i port_H5 -z conv,ip`:
```
================================================================================
IPv4 Conversations
Filter:<No Filter>
                                               |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                                               | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
1.0.0.2              <-> 1.4.0.2                  761     48650    1169   1768426    1930   1817076     0.000000000        14.4751
1.0.0.3              <-> 1.4.0.2                  448     32232     670   1014380    1118   1046612     0.006047384        14.4751
1.4.0.2              <-> 1.7.0.3                  554    838756     375     28290     929    867046     0.018177772        14.4509
1.3.0.1              <-> 1.4.0.2                    0         0       1        82       1        82     0.496585293         0.0000
================================================================================
```

We see that some traffic originates from `1.7.0.3`.
This could be a source of the attack.


We next check what traffic comes from the hosts `h3` and `h4` of the network.
We do that with the command
```
mx r2 tshark -i port_S2 -z conv,ip
```

Indeed, there is a lot of traffic:
```
================================================================================
IPv4 Conversations
Filter:<No Filter>
                                               |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                                               | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
1.0.0.2              <-> 1.4.0.2                    0         0    1411   2136254    1411   2136254     0.002431566         6.5986
1.4.0.2              <-> 1.7.0.3                  615    931110     131      9754     746    940864     0.058022561         6.4898
1.0.0.3              <-> 1.4.0.2                    0         0     702   1062828     702   1062828     0.000000000         6.6054
1.0.0.3              <-> 1.7.0.2                  189     10206     274    414836     463    425042     0.048302160         6.5405
1.0.0.2              <-> 1.7.0.2                  168      9072     274    414836     442    423908     0.084629441         6.4997
================================================================================
```

However, we do not want to allow any traffic originating from the hosts connected to `s2`.
Hence, we block all traffic coming from `s1` with one of these IP addresses:
```
mx r2 iptables -A FORWARD -i port_S2 -j DROP
```

### Defending against the next portion of the attack traffic

Next, we must check what traffic originates from the hosts connected to `s1`.

For this, we look at the flows at the closest router `r1` with the command `mx r1 tshark -i port_S1 -z conv,ip`

```
================================================================================
IPv4 Conversations
Filter:<No Filter>
                                               |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                                               | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
1.0.0.2              <-> 1.4.0.2                  205     15358    1577   2387578    1782   2402936     0.000000000         7.4134
1.0.0.3              <-> 1.4.0.2                  213     15790    1493   2260402    1706   2276192     0.002423245         7.4170
1.0.0.3              <-> 1.7.0.2                    0         0    1567   2372438    1567   2372438     0.004844852         7.4162
1.0.0.2              <-> 1.7.0.2                    0         0    1491   2257374    1491   2257374     0.007268726         7.4101
================================================================================
```

We see that there is traffic from `1.0.0.2` to `h5` (`1.4.0.2`), but there is also traffic from `1.0.0.3` and traffic towards `1.7.0.2`!
We therefore block all traffic with source IP address `1.0.0.3`.

The first part we can do by blocking traffic not originating from `h1`'s IP address:
```
mx r1 iptables -A FORWARD --src 1.0.0.3 -j DROP
```

### Defending the remaining portion of the attack traffic

We rather do not want to block all traffic towards `1.7.0.2` as this makes `h3` unavailable to `h2` (see discussion in previous task).
We therefore check again the conversations on the IP level:
```
================================================================================
Ethernet Conversations
Filter:<No Filter>
                                               |       <-      | |       ->      | |     Total     |    Relative    |   Duration   |
                                               | Frames  Bytes | | Frames  Bytes | | Frames  Bytes |      Start     |              |
60:0d:be:ef:ca:fe    <-> be:60:0d:be:60:0d       2586   3915204     165     10518    2751   3925722     0.000000000         3.1310
================================================================================
```

Wow, the attacker not only spoofs the MAC address but also the IP address!

We therefore cannot have rules based solely on the IP address and the MAC address.

Consequently, we must dig deeper.
Let us inspect the payload with the command
```
mx r1 tshark -i port_S1 -T fields -e data
```

We see that many payloads only consist of zeroes:
```
...

00000000000000000000000000000[...]0000000000000000000000000000000000000000000000

2740 packets captured
```

Could this be benign traffic? Rather not, why would a benign host send such data?
We therefore filter on the payload.
We can do this with the command
```
mx r1 iptables -A FORWARD -m string --algo bm --hex-string '"|00000000000000000000000000000000|"' -j DROP
```

This command checks whether the packet contains 2^16 consecutive `0`s (note that we match on a hexadecimal string).

Of course, this introduces potential false positives, i.e., we could drop benign traffic.
Considering that the benign traffic contains encrypted data, the probability that there are 2^16 zeroes is negligible.
As computing this probability is not the core part of this exercise, we refer the interested reader to [this stackexchange answer](https://math.stackexchange.com/a/2045714) and provide a small python script for you to play around with.


```python
n_consecutive_zeroes = 16 * 4
n_bits = 1500 * 8  # Rough payload size, might be a bit less
n_packets = round(10**7 / n_bits)  # 10 Mb divided by the number of bits per packet

b = []

for m in range(1, n_consecutive_zeroes):
    b.append(2**m)

b.append(2**n_consecutive_zeroes - 1)
for i in range(n_consecutive_zeroes, n_bits):
    b.append(sum(b[-n_consecutive_zeroes:]))

a_n = 2**n_bits - b[-1]
p = a_n / 2**n_bits # probability that there are consecutive zeroes

print(
    f"The probability that a random bitstring of length {n_bits} "
    f"has {n_consecutive_zeroes} consecutive zeroes is {p}."
)

print(
    f"The probability that  packet out of {n_packets} has {n_consecutive_zeroes} consecutive zeroes "
    f"is {1 - (1-p)**n_packets}."
)
```

### Questions

1. **How realistic is the attack scenario?**

   A real-world attacker can easily spoof the MAC address.
   In addition to that, the attacker may launch [ARP poisoning attacks](https://www.varonis.com/blog/arp-poisoning).

2. **Would your defense be acceptable in the real world? Why or why not?**

   It would be pretty easy for the attacker also to send random bytes.
   Hence, our defense would no longer work.

3. **Do you see other (potentially better) ways to defend against this attack?**

   In case the attacker also randomizes the payload, we could match the packet size as the attacker potentially sends larger packets than a regular application.

## Final thoughts

1. **What scenarios could you "defend" against by adding new links between routers and cleverly rerouting the traffic?**
   The scenarios in which the bottleneck is at a link connecting a router (`(r1, r2), (r1, r3)`).
   If, e.g., link `(r1,r2)` is congested (actually happening in all of the above attacks), adding the new link `(r1, r3)` and re-routing some traffic (e.g., only benign one) over it can help.

   However, if the congestion is happening _before_ reaching a router (i.e., in `(s1, r1)`) or _after_ the last router (i.e., in `(r3, h5)`), then new links between routers cannot help.
2. **Assume that you could also control the switches `s1` and `s2`.**
   - **How does this help to defend the attacks?**
     We can now directly disconnect malicious hosts from the network without causing any collateral damage.
   - **How can you ensure that no host can do MAC or IP address spoofing?**
     The simplest way is to assign a static MAC and IP address to every host.
     Then, the switches must have static ARP forwarding tables.
     For every incoming packet, the switch must check whether the source MAC and the source IP address match the address assigned to the device connected to the corresponding interface.
     If not, then the packet must be dropped.

   - **Would it be possible to defend solely in the switches?**
     Yes, you could simply disconnect the malicious hosts (e.g., `h2`).
     However, applying the same filters as in the routers is also possible.
   - **_Optional_: Write down the pseudo-code to defend against the attacks.**
     See the [Matrix chat room](https://matrix.to/#/#adv-net-23:studentchat.ethz.ch) for possible solutions and discussions.
