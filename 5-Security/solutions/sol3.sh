#!/bin/sh

mx r1 iptables -A FORWARD -m string --algo bm --hex-string '"|00000000000000000000000000000000|"' -j DROP
mx r2 iptables -A FORWARD -i port_S2 -j DROP

# The following rules could be added, but are redundant:
# mx r1 iptables -A FORWARD -p udp -j DROP
# mx r1 iptables -A FORWARD --dst 1.7.0.2 -j DROP
# mx r1 iptables -A FORWARD --src 1.0.0.3 -j DROP
