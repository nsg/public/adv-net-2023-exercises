#!/bin/sh
#
# This script lets you to defend against the DoS attack 3.
# The syntax is the following:
# mx [ROUTER] iptables -A FORWARD [RULE]
#
# A small example (that obvisouly does not defend):
# mx r3 iptables -A FORWARD -p tcp -j DROP
#
# Be aware that in case you need to have quotes, use nested quotes:
# mx r1 tshark -f '"host 1.0.0.2"'
