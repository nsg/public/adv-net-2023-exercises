"""
Filename: verification.py

Description:
    This file is the entry point for the exercise.

    We first define a network model and a specification.
    We then verify the specification on the network model and print the result.
    If the specification is violated, we also print a formatted counterexample.

"""

from z3 import Not, And, Or, If, Implies, Solver, sat

from route_map import RouteMap, RmItem
from network import Network, Solution, SessionType
from routing_semantics import RoutingEquations, TransformedRoute

net = Network()

# add the routers of the network
net.add_external("Customer")
net.add_router("A")
net.add_router("B")
net.add_router("C")
net.add_router("D")
net.add_external("Provider")
net.add_external("Peer")

# define the network topology
net.add_link("A", "B", 1)
net.add_link("A", "C", 1)
net.add_link("B", "D", 3)
net.add_link("C", "D", 2)
net.add_link("B", "Provider", 1)
net.add_link("C", "Customer", 1)
net.add_link("D", "Peer", 1)

# network configuration: add the BGP sessions
net.add_session("A", "B", SessionType.IBGP)
net.add_session("A", "C", SessionType.IBGP)
net.add_session("A", "D", SessionType.IBGP)
net.add_session("B", "C", SessionType.IBGP)
net.add_session("B", "D", SessionType.IBGP)
net.add_session("C", "D", SessionType.IBGP)
net.add_session("B", "Provider", SessionType.EBGP)
net.add_session("C", "Customer", SessionType.EBGP)
net.add_session("D", "Peer", SessionType.EBGP)

# Ingress route maps (tag routes from peers and providers with community 2)
net.set_route_map("Provider", "B", RouteMap([RmItem(True, None, None, 2)]))
net.set_route_map("Peer", "D", RouteMap([RmItem(True, None, None, 2)]))
# Egress route maps (deny routes with commmunity 2 towards the peer and the provider).
net.set_route_map("B", "Provider", RouteMap([RmItem(False, 2, None, None)]))
net.set_route_map("D", "Peer", RouteMap([RmItem(False, 2, None, None)]))


def Spec(net):
    Provider = net.Routers["Provider"]
    Peer = net.Routers["Peer"]
    B = net.Routers["B"]
    D = net.Routers["D"]

    SpecProviderToPeer = Implies(
        And(B.SelectsFrom == Provider.id, D.SelectsFrom == B.id),
        Not(TransformedRoute(net, "D", "Peer").Available),
    )

    SpecPeerToProvider = Implies(
        And(D.SelectsFrom == Peer.id, B.SelectsFrom == D.id),
        Not(TransformedRoute(net, "B", "Provider").Available),
    )

    return And(SpecProviderToPeer, SpecPeerToProvider)


# Perform the verification
s = Solver()
# build the network model
s.add(RoutingEquations(net))
# add the specification (as an inverse to find counterexamples)
s.add(Not(Spec(net)))

# run the SMT solver
if s.check() == sat:
    # the solver found a way to violate the spec!
    print("Specification is violated!")

    # get the solution
    sol = Solution(s)
    # print the network
    print(net.fmt(sol))

else:
    # the solver did not find a way to violate the spec!
    print("Specification is satisfied!")
