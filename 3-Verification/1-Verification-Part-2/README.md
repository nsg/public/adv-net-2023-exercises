# Network Verification: Week 2

This is a continuation of Week 1 exercise.
If you prefer, you can keep your own solution from last week and continue with it.

## Task 3: Route Maps

By now, you have implemented the BGP route selection process with a few attributes.
However, the `Community` attribute is unused, and `LocalPref` always takes its default value.
In this task, you will implement route maps that can influence route propagation and selection based on the `Community`.

### Subtask 3.1: Understanding the semantics

Current BGP routers have a very complex language to express route maps (e.g., they allow matching on the AS path using regular expressions).
This exercise will only consider route maps with simple semantics.
Read the documentation of the `RouteMap` and `RmItem` classes in the file `route_map.py`, and understand the route-map semantics.

Let's say we want to always prefer routes from the `Customer` over routes from the `Provider`.
How can you configure route maps to implement that behavior?
Configure `net` in `verification.py` to reflect that configuration.

*Hint*:
The following Python snippet will add a route map from router `A` to router `B` that denies routes with community 5, and sets a higher local preference to routes with community 6:
```python
net.set_route_map("A", "B", RouteMap([
    #      allow, condition, local_pref, community
    RmItem(False,         5,       None,      None)
    RmItem( True,         6,        100,      None)
]))
```

### Subtask 3.2: Implement route-map items

The skeleton doesn't yet contain the code to apply route-map items to symbolic routes.
In the file `route_map.py`, modify the function `RmItem.__call__` to implement our route-map semantics.

The function `__call__` is a magic function in Python that you can execute by writing `item(Route)`.
As an input, it takes the original `BgpRoute` and should return the modified `BgpRoute`.
You can directly assign the route attributes symbolic expressions in terms of the input route.
For instance, to modify the `Route.LocalPref`, you can use:

```python
Cond = ...  # A symbolic expression that is True if self matches the Route and if LocalPref should be modified.
Route.LocalPref = If(Cond, self.local_pref, Route.LocalPref)
```

This statement above will ensure that the returned `Route` has the `LocalPref` set to `self.local_pref` if `self` matches the `Route` (and if the `LocalPref` should be modified), or it will keep its original value.

*Hint*: Consider what happens when multiple route-map items are chained together.
If a route-map item denies a route (with `item.allow == False`), then no later route-map item can "undo" that decision.
In other words, if an item denies a route, the route will *early-exit* the route map.
In SMT, we do not know beforehand if an item denies a route; this depends on the symbolic variables.
Instead, the route traverses all route-map items (see the implementation of `RouteMap.__call__`).
Each route-map item must check if an earlier route-map item denied the route.
We use `Route.Available` to indicate whether `Route` was already denied.

*Validate your solution*:
Import the `Task_3` as `Spec` in `verification.py`.
```python
from tasks import Task_3 as Spec
```
After implementing `RmItem.__call__` and changing the configuration (from Subtask 3.1), the specification should be satisfied.

## Task 4: Writing the specification

By now, you have implemented a BGP verifier to verify BGP configurations, including route maps.
In the last task, you will write design specifications to express your intent and write a configuration to implement your specification.
From now on, you will only need to modify the file `verification.py`.

### Subtask 4.1: Warm-up

Write a specification that describes the following behavior:

> The network prefers routes from the `Customer` unless the `Provider` advertises a route with community 5.

Then, design a configuration that satisfies the specification.
Use the counter-examples from the verifier to find a valid configuration.

*Hint*: Ensure you cover all cases (e.g., what happens if the `Customer` advertises a route but not the `Provider`?).
Look at the specifications from the previous task.

### Subtask 4.2: Extending the network

For the next two subtasks, we introduce a third external network called `Peer`.
In `verification.py`, add a new external router called `Peer` connected to `D` (add a link and an eBGP session).

### Subtask 4.3: No transit between the peer and the provider

Write a specification that describes the following behavior:

> Routes from the `Peer` must not be advertised to the `Provider` and vice-versa.

Then, design a configuration that satisfies the specification.
Use the counter-examples that the verifier generates to find a valid configuration.

*Hint*: We do not model the route selection process for external routers.
Nevertheless, you can use the function `TransformedRoute(net, src, dst)` to get the route that `src` advertises to `dst`.

### Subtask 4.4: Full transit from the customer to others

*Extend* the specification from Subtask 4.3.
In addition to the no-transit specification between the `Peer` and the `Provider`, ensure that:

> The `Customer`'s route is always advertised to both the `Peer` and the `Provider`.

Then, design a configuration that satisfies the specification.
Use the counter-examples that the verifier generates to find a valid configuration.

### Question 4.5

Give a naive configuration that would satisfy your specification from Subtask 4.4, but that does not provide reachability in all routing environments.
Describe what aspects of the network your current specification does not cover if your goal is to implement the well-known *customer-peer-provider* relationship.
