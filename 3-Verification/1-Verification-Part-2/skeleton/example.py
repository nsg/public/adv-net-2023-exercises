from network import Network, SessionType
from routing_semantics import RoutingEquations


net = Network()

# add the routers of the network
net.add_external("Customer")
net.add_router("A")
net.add_router("B")
net.add_router("C")
net.add_router("D")
net.add_external("Provider")

# define the network topology
net.add_link("A", "B", 1)
net.add_link("A", "C", 1)
net.add_link("B", "D", 3)
net.add_link("C", "D", 3)
net.add_link("B", "Provider", 1)
net.add_link("C", "Customer", 1)

# network configuration: add the BGP sessions
net.add_session("A", "B", SessionType.IBGP)
net.add_session("A", "C", SessionType.IBGP)
net.add_session("A", "D", SessionType.IBGP)
net.add_session("B", "C", SessionType.IBGP)
net.add_session("B", "D", SessionType.IBGP)
net.add_session("C", "D", SessionType.IBGP)
net.add_session("B", "Provider", SessionType.EBGP)
net.add_session("C", "Customer", SessionType.EBGP)

NetworkModel = RoutingEquations(net)

Customer = net.Routers["Customer"]
Provider = net.Routers["Provider"]
A = net.Routers["A"]
B = net.Routers["B"]
C = net.Routers["C"]
D = net.Routers["D"]
