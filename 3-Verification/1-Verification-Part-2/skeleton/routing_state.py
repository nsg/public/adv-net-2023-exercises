"""
Filename: routing_state.py

Description:
    This file defines classes related to BGP routing states.

    - BgpRoute: A class that represents a symbolic BGP route.
    - Router: A class that represents an internal router that selects a symbolic BGP route.
    - ExternalRouter: A class that represents an external router that advertises a symbolic BGP
      route.
    
"""

from z3 import Bool, Int, Not, Or, And
from dataclasses import dataclass


@dataclass
class BgpRoute:
    """
    A BgoRoute contains a set of symbolic attributes that describe a BGP route.

    - Available (Bool): A symbolic boolean variable encoding whether the route is present or not.
      If `Available` is `False`, all attributes in the route are invalid. Otherwise, the attributes
      are valid and the route can be selected.
    - LocalPref (Int): A symbolic integer that encodes the BGP local preference value.
    - AsPathLen (Int): A symbolic integer that encodes the length of the AS path.
    - NextHop (Int): A symbolic integer that encodes the BGP next-hop attribute.
      After an internal router selects a route, the router will choose an IGP next-hop based on
      the IGP cost to forward traffic towards this next-hop.
    - IgpCost (Int): A symbolic integer that encodes the IGP cost to reach `NextHop`.
    - Community (Int): A symbolic integer that encodes one BGP community value.
    """

    def __init__(self, label):
        """Initializes the symbolic BGP route."""
        self.Available = Bool(f"{label}.Route.Available")
        self.LocalPref = Int(f"{label}.Route.LocalPref")
        self.AsPathLen = Int(f"{label}.Route.AsPathLen")
        self.IgpCost = Int(f"{label}.Route.IgpCost")
        self.NextHop = Int(f"{label}.Route.NextHop")
        self.Community = Int(f"{label}.Route.Community")

    def fmt(self, net, sol):
        """
        Returns a formatted string with the concrete BGP attribute values assigned by the solution.

        - net (Network): The network model.
        - sol (Solution): The solution to the network model.
        """
        # Compute the concrete values for `self` by calling the `solution`. THis will return a new
        # `BgpRoute` object that has concrete values assigned to all the attributes.
        r = sol(self)
        if r.Available:
            return (
                "{"
                f"Local Pref: {r.LocalPref}, "
                f"AS path len: {r.AsPathLen}, "
                f"IGP cost: {r.IgpCost}, "
                f"nh: {net.routers_rev[r.NextHop.as_long()]}, "
                f"community: {r.Community}, "
                "}"
            )
        else:
            return "{}"

    def __eq__(self, other):
        """
        Returns a boolean expression that encodes when two BGP routes are equal.

        The boolean expression is evaluated to `True` if all attributes except `Available` are
        equal. This is a python magic function that is called in `self == other`, e.g., in the
        steady state equations.

        - other (BgpRoute): The other BGP route to compare with.
        """
        return And(
            self.LocalPref == other.LocalPref,
            self.AsPathLen == other.AsPathLen,
            self.NextHop == other.NextHop,
            self.IgpCost == other.IgpCost,
            self.Community == other.Community,
        )

    def __gt__(self, other):
        """
        Returns a boolean expression that encodes when the BGP route `self` is preferred over
        `other` according to the BGP decision process.

        This is a python magic function that is called in `self > other`.

        - other (BgpRoute): The other BGP route to compare with.
        """
        return Or(
            self.LocalPref > other.LocalPref,
            And(
                self.LocalPref == other.LocalPref,
                self.AsPathLen < other.AsPathLen,
            ),
            And(
                self.LocalPref == other.LocalPref,
                self.AsPathLen == other.AsPathLen,
                self.IgpCost < other.IgpCost,
            ),
            And(
                self.LocalPref == other.LocalPref,
                self.AsPathLen == other.AsPathLen,
                self.IgpCost == other.IgpCost,
                self.NextHop < other.NextHop,
            ),
        )

    def __le__(self, other):
        """
        Returns a boolean expression that encodes when `self <= other`.

        - other (BgpRoute): The other BGP route to compare with.
        """

        return Not(self.__gt__(other))

    def __ge__(self, other):
        """
        Returns a boolean expression that encodes when `self >= other`.

        - other (BgpRoute): The other BGP route to compare with.
        """
        return Or(self.__eq__(other), self.__gt__(other))

    def __lt__(self, other):
        """
        Returns a boolean expression that encodes when `self < other`.

        - other (BgpRoute): The other BGP route to compare with.
        """
        return Not(self.__ge__(other))

    def __ne__(self, other):
        """
        Returns a boolean expression that encodes when `self != other`.

        - other (BgpRoute): The other BGP route to compare with.
        """
        return Not(self.__eq__(other))


@dataclass
class Router:
    """
    An internal router contains a set of symbolic attributes that describe its routing state.

    - name (str): A string that represents the name of the internal router.
    - id (int): An integer that represents that router Id.
    - Route (BgpRoute): The symbolic BGP route that the internal router selects.
    - SelectsFrom (Int): A symbolic integer that encodes the router Id where the route is selected
      from.
    """

    def __init__(self, name, id):
        """
        Initializes the internal router.

        - name (str): The name of the router.
        - id (int): The router Id.
        """
        self.name = name
        self.id = id
        self.Route = BgpRoute(f"r{name}")
        self.SelectsFrom = Int(f"r{name}.SelectsFrom")

    def fmt(self, net, sol):
        """
        Returns a formatted string with the concrete field values assigned by the solution.

        - net (Network): The network model.
        - sol (Solution): The solution to the network model.
        """
        if sol(self.Route.Available):
            return (
                "{"
                f"id: {self.id}, "
                f"route: {self.Route.fmt(net, sol)}, "
                f"from: {net.routers_rev[sol(self.SelectsFrom).as_long()]}"
                "}"
            )
        return "{}"

    def is_internal(self):
        """Returns True if the router is an internal router."""
        return True

    def is_external(self):
        """Returns True if the router is an external router."""
        return False

    def clone(self, idx):
        """
        Clones the router with new symbolic variable names.

        - idx (int): The index that decides new symbolic variable names.
        """
        return Router(f"{self.name}:{idx}", self.id)


@dataclass
class ExternalRouter:
    """
    An external router contains a symbolic BGP route that it advertises.

    - name (str): A string that represents the name of the external router.
    - id (int): An integer that represents that router Id.
    - Route (BgpRoute): The symbolic BGP route that the external router advertises.
    """

    def __init__(self, name, id):
        self.name = name
        self.id = id
        self.Route = BgpRoute(name)

    def fmt(self, net, sol):
        """return a string with the values from the solution."""
        return self.Route.fmt(net, sol)

    def is_internal(self):
        return False

    def is_external(self):
        return True

    def clone(self, idx):
        """Create a new copy of the router with different variable names."""
        return ExternalRouter(f"{self.name}:{idx}", self.id)
