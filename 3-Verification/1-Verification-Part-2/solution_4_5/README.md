# Solutions for Task 4.5

Give a naive configuration that would satisfy your specification from Subtask 4.4, but that does not provide reachability in all routing environments.

> A network that only accepts routes from the customer, but denies all routes from the provider will satisfy the specification:
> ```python
> net.set_route_map("Peer",     "D", RouteMap([RmItem(False, None, None, None)]))
> net.set_route_map("Provider", "B", RouteMap([RmItem(False, None, None, None)]))
> ```

Describe what aspects of the network your current specification does not cover if your goal is to implement the well-known *customer-peer-provider* relationship.

> To follow the business relationships, the network should ensure two rules:
> 1. prefer the route from a customer, then from a peer, and last from a provider, and
> 2. only advertise customer routes to peers and providers, but advertise all routes to customers (c.f., the following table:)
>
>| from\to  | customer | peer  | provider |
>| :------: | :------: | :---: | :------: |
>| customer |    ✅     |   ✅   |    ✅     |
>|   peer   |    ✅     |   ❌   |    ❌     |
>| provider |    ✅     |   ❌   |    ❌     |
>
> 
> The specification from 4.3 and 4.4 only restrict the routes advertised to the `Peer` and `Provider`.
> It does not describe the routes that the network must advertise to the `Customer`.
> Further, it does not verify the first rule that routes from the `Peer` are preferred over the one from the `Provider`.
> The routes from the customer must be preferred over those from the `Provider` and `Peer` because otherwise, the `Customer`'s route would not be advertised to the `Peer` or `Provider` (c.f., Specification from Task 4.4).
