# Block 3: Verification

## Development Environment Setup

You will use the same VM as in the previous two exercises. Refer to [last week's exercise](https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises/-/blob/main/2-Programmability/README.md?ref_type=heads#access-your-own-vm) for detailed instructions.

You will need the [z3 solver](https://theory.stanford.edu/~nikolaj/programmingz3.html) and additional python libraries. Run the following on your VM to install them:

```
sudo apt-get install python3-z3 python3-networkx
```

## Exercises

In this section, we provide links to the weekly exercises.

If you have not already done so, clone this repository in the `p4` user home directory, as illustrated below:

```
cd /home/p4/
git clone https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises.git
```

**Important** Pull the new tasks from the repository before every exercise session:

```
cd /home/p4/adv-net-2023-exercises
git pull
```

### General Structure


- [Week 1: Network Verification](./1-Verification)
- [Week 2: Network Verification (Part 2)](./1-Verification-Part-2)
- [Week 3: Configuration Synthesis](./2-Synthesis)
