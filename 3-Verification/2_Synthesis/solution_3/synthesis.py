from z3 import Not, And, Or, If, Implies, Solver, sat

from route_map import SymbolicRm
from network import Network, Solution, SessionType
from routing_semantics import RoutingEquations, TransformedRoute


net = Network()

# add the routers of the network
net.add_external("Cust")
net.add_router("A")
net.add_router("B")
net.add_router("C")
net.add_router("D")
net.add_external("Prov")
net.add_external("Peer")

# define the network topology
net.add_link("A", "B", 1)
net.add_link("A", "C", 1)
net.add_link("B", "D", 3)
net.add_link("C", "D", 2)
net.add_link("B", "Prov", 1)
net.add_link("C", "Cust", 1)
net.add_link("D", "Peer", 1)

# network configuration: add the BGP sessions
net.add_session("A", "B", SessionType.IBGP)
net.add_session("A", "C", SessionType.IBGP)
net.add_session("A", "D", SessionType.IBGP)
net.add_session("B", "C", SessionType.IBGP)
net.add_session("B", "D", SessionType.IBGP)
net.add_session("C", "D", SessionType.IBGP)
net.add_session("B", "Prov", SessionType.EBGP)
net.add_session("C", "Cust", SessionType.EBGP)
net.add_session("D", "Peer", SessionType.EBGP)

net.set_route_map("Cust", "C", SymbolicRm("CustToC", 1))
net.set_route_map("Prov", "B", SymbolicRm("ProvToB", 1))
net.set_route_map("Peer", "D", SymbolicRm("PeerToD", 1))

net.set_route_map("C", "Cust", SymbolicRm("CToCust", 1))
net.set_route_map("B", "Prov", SymbolicRm("BToProv", 1))
net.set_route_map("D", "Peer", SymbolicRm("DToPeer", 1))


# Assert that Cust is preferred over the Prov unless the Prov advertises a route with
# community 5
def Spec(net):
    Prov = net.Routers["Prov"]
    Cust = net.Routers["Cust"]
    Peer = net.Routers["Peer"]
    B = net.Routers["B"]
    C = net.Routers["C"]
    D = net.Routers["D"]

    SpecProvToPeer = Implies(
        And(B.SelectsFrom == Prov.id, D.SelectsFrom == B.id),
        Not(TransformedRoute(net, "D", "Peer").Available),
    )

    SpecPeerToProv = Implies(
        And(D.SelectsFrom == Peer.id, B.SelectsFrom == D.id),
        Not(TransformedRoute(net, "B", "Prov").Available),
    )

    SpecCustToPeer = Implies(
        Cust.Route.Available,
        And(
            C.SelectsFrom == Cust.id,
            D.SelectsFrom == C.id,
            TransformedRoute(net, "D", "Peer").Available,
        ),
    )

    SpecCustToProv = Implies(
        Cust.Route.Available,
        And(
            C.SelectsFrom == Cust.id,
            B.SelectsFrom == C.id,
            TransformedRoute(net, "B", "Prov").Available,
        ),
    )

    # full transit from Peer or Prov to Cust (when the Cust does not advertise a route)
    SpecProvOrPeerToCust = Implies(
        And(
            Not(Cust.Route.Available),
            Or(Peer.Route.Available, Prov.Route.Available),
        ),
        TransformedRoute(net, "C", "Cust").Available,
    )

    # Prefer the routes from the Peer over the ones from the Prov
    SpecPreferPeerOverProv = Implies(
        And(
            Not(Cust.Route.Available),
            Peer.Route.Available,
            Prov.Route.Available,
        ),
        B.SelectsFrom == D.id,
    )

    return And(
        SpecProvToPeer,
        SpecPeerToProv,
        SpecCustToPeer,
        SpecCustToProv,
        SpecProvOrPeerToCust,
        SpecPreferPeerOverProv,
    )


def Synthesis(net, env):
    """
    Synthesizes a new configuration that satisfies all environemnts. The configuraiton shoulkd have
    the form of net.get_route_maps().
    """
    s = Solver()

    for idx, x in enumerate(env):
        env_net = net.clone(idx)
        # replace the routing inputs by the actual counter example
        for ext, Route in x.items():
            env_net.Routers[ext].Route = Route
        s.add(RoutingEquations(env_net))
        s.add(Spec(env_net))

    # check whether there exists a configuraiton that satisfies all counter examples
    if s.check() != sat:
        # No assignment of configuration parameters can satisfy all counter examples. This means
        # that there does not exist a solution to the synthesis problem. This is the place where we
        # would implement SyGuS!
        raise RuntimeError("There is no solution to the synthesis problem!")

    # Get the synthesize a configuration for the current set of environments
    c = net.get_route_maps(s)
    print(
        "\nCurrent problem is solved by:\n"
        + "\n".join(f"{RouteMap}" for RouteMap in c.values())
    )

    return c


def Verification(net, c):
    """
    Verify that the configuration is correct. Either return None indicating that the configuration
    is correct, or return a counter example. The counter example should have the form of
    net.get_environment()
    """
    # clone the network so we don't modify it!
    net = net.clone(0)

    s = Solver()
    # replace the configuration (route maps) by the actual configuration synthesized by s_synth.
    net.RouteMaps = c
    # build s_verif
    s.add(RoutingEquations(net))
    s.add(Not(Spec(net)))
    # check if there exists a counter-example
    if s.check() != sat:
        # found a valid solution! Break out of the loop
        print("\nFound a configuration!")
        return None

    # counter-example found. Extract the environment for that counter-example
    x = net.get_environment(s)
    print(
        "\nCurrent configuration does not work with environment:\n"
        + "\n".join(f"{e}: {r}" for e, r in x.items())
    )
    return x


# prepare the synth solver which we make larger in every iteration.
env = []

# main CEGIS loop
for i in range(0, 100):
    print(f"\n\nIteration {i + 1}\n===========")

    # Synthesis phase
    c = Synthesis(net, env)

    # Verification phase
    x = Verification(net, c)

    if x is None:
        print("Solution found!")
        break

    env.append(x)
