# Configuration Synthesis

In this exercise, you will build a configuration synthesizer.
Given the specification, the synthesizer should find valid BGP route maps with CEGIS to satisfy the specification.

## Task 1: Parameterizing route maps
In last week's exercise, you implemented the semantics of route maps.
This week, you will parameterize them with symbolic variables and later synthesize them to satisfy the specification.

In the first task, you are given an unfinished skeleton of `route_map.py`.
Compared with last week's exercise, the `RouteMapItem` and `RouteMap` are replaced with `SymbolicRmItem` and `SymbolicRm`, respectively. 

### Subtask 1.1: Understanding the parameters

Let's start with understanding how the skeleton parameterizes the route maps and route-map items.

- First, look at the constructor function `SymbolicRmItem.__init__` of symbolic route-map items.
  How do the variables relate to last week's [route maps](https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises/-/blob/main/3-Verification/1-Verification-Part-2/solution_3_2/route_map.py?ref_type=heads#L94-118)?
- Second, look at the constructor `SymbolicRm.__init__`.
  What is that argument `num`?

- Finally, look at `synthesis.py`.
  We use the same topology as last week's exercise, including the `Peer` (but this time, we call the customer `Cust` and the provider `Prov`).
  We also use the same [specification](https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises/-/blob/main/3-Verification/1-Verification-Part-2/solution_4_1/verification.py?ref_type=heads#L53-81) as last week's [Task 4.1](https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises/-/tree/main/3-Verification/1-Verification-Part-2?ref_type=heads#subtask-41-warm-up).
  What configuration parameters (i.e., variables that describe route maps) do we have?

You do not need to implement anything yet in this task.

### Subtask 1.2: Implementing parameterized route-map items

Implement the parameterized route-map items by completing the function `SymbolicRmItem.__call__`.

*Hint*: You can no longer use the python `if` statement, as in [`RmItem`](https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises/-/blob/main/3-Verification/1-Verification-Part-2/solution_3_2/route_map.py?ref_type=heads#L94-118).
Instead, you must assign all route attributes to symbolic expressions using `If`.

## Task 2: Implementing the CEGIS algorithm
In this task, you will implement the CEGIS algorithm to synthesize configurations that satisfy the specification.

As you have seen in the lecture, the CEGIS loop consists of two phases: Synthesis and Verification.
In Synthesis, we synthesize a configuration (i.e., route maps) `c` to satisfy the specification for all environments `env`, i.e., a list of concrete routing inputs.
In Verification, we verify whether a configuration `c` satisfies the specification under *any* routing input. 
If the verification fails, the solver returns a counter-example `x`, i.e., a set of concrete routes advertised by external routers.
`x` is then appended in `env` and will be considered in the next iteration.
If the verification succeeds, we have found the valid configuration `c`.

### Subtask 2.1: Verification phase
Let's start with Verification.
Complete the function `Verification(net, c)` in the skeleton `synthesis.py`.

To test `Verification(net, c)`, uncomment the block right after the function `Verification` and ensure that your code returns a counter-example.
Comment-out the test afterward.

Here are a few pointers in case you get stuck:
- As given in the skeleton, the first statement `net = net.clone(0)` in `Verification` creates a clone of the network.
  This is such that you can modify `net` without affecting the outer CEGIS loop.
- The function `Verification` must return either a counter-example `x` (i.e., concrete routing inputs) *or* `None` if the configuration satisfies the specification for all routing inputs.
  The skeleton already contains the code to generate the counter-example `x = net.get_environment(s)`.
- The argument `c` is the synthesized configuration.
  It is a dictionary, mapping BGP sessions to concrete `SymbolicRm`s (all attributes have concrete values).
  The argument `c` has the same type as `net.RouteMaps`.
  You can make the network use the concrete configuration by setting `net.RouteMaps = c`.


### Subtask 2.2: Synthesis phase
Complete the function `Synthesis(net, env)` in the skeleton `synthesis.py`.

To test `Synthesis(net, c)`, uncomment the block right after the function `Synthesis` and ensure that your code returns a configuration.
Comment-out the test afterward.

Here are a few pointers in case you get stuck:
- At the start of `Synthesis`, the skeleton already iterates over all counter-examples in the argument `env`.
  For each counter-example `x` with index `idx`, we clone the network (using `env_net = net.clone(idx)`).
  `env_net` is a copy of the network, where all the variables of the routing input and routing state are duplicates with different names, e.g., `rA:1.Route.Available`.
  The configuration variables are *not* duplicated.
- The function `Synthesis` must return a configuration `c` that satisfies the specification under all given counter-examples in `env`.
  The skeleton already contains the code to generate the configuration `c = net.get_route_maps(s)`.
- The argument `env` is a list of counter-examples.
  Each counter-example `x` maps all external routers to a concrete route (a `BgpRoute` with concrete values instead of variables).
  You can make the network use the counter-example `x` using the following loop:
  ```python
  for ext, Route in x.items():
      env_net.Routers[ext].Route = x[ext]
  ```

### Subtask 2.3: Implement the CEGIS loop
Combine your two functions, `Specification` and `Synthesis`, to implement the CEGIS algorithm.
Do so at the end of `synthesis.py` (the skeleton already contains the main loop).

Once you have implemented the CEGIS algorithm, you can run `python synthesis.py` to see whether you will synthesize a configuration that satisfies the specification `Spec`.
This should take around 8 CEGIS iterations.
Ensure that the configuration you synthesize is correct (prefer the customer's route over the provider's route unless the provider advertises a community `5`).


## Task 3: Synthesizing configurations
In this final task, we will put everything together:
You will write a complex specification in `Spec(net)`.
Then, you will use your synthesizer to find a configuration that satisfies the specification.

### Subtask 3.1: Writing the specification
Your task is to write the specification for the well-known [*customer-peer-provider* relationship](https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises/-/tree/main/3-Verification/1-Verification-Part-2/solution_4_5?ref_type=heads#solutions-for-task-45) in `synthesis.py`.
You can use the solution of last week's [task 4.4](https://gitlab.ethz.ch/nsg/public/adv-net-2023-exercises/-/blob/main/3-Verification/1-Verification-Part-2/solution_4_4/verification.py?ref_type=heads#L59-100) as a starting point (but you can follow a different approach).

Run `python verification.py` again. CEGIS should yield that the current configuration parameters are not expressive enough to solve this specification.

### Subtask 3.2: Running the synthesizer
Think about why the current configuration parameters are not expressive enough for this specification.
Extend the routing parameters available to the synthesizer by modifying `net` (above the function `Spec`).
Rerun CEGIS and validate that the synthesized configuration actually implements the specification.
