"""
Filename: route-map.py

Description:
    This file defines classes related to BGP routing states route-maps.

    - RmItem: A class that represents an individual route-map item.
    - RouteMap: A class that represents a route-map.


"""

from z3 import Bool, Int, If, And, Or, Not
from dataclasses import dataclass
from copy import copy


@dataclass
class SymbolicRmItem:
    """
    A symbolic route map item that is equivalent to an individual route-map entry. This class is the
    parametrized (symbolic) version of `RmItem`, and has the same semantics. See last week's
    `route_map.py` for reference.

    - MatchAll (Bool): If `True`, then match everything. Otherwise, use the `Condition`.
    - Condition (Int): The condition on the community (only if MatchAll is False).
    - SetLocalPref (Bool): If `True`, then modify the local preference.
    - LocalPref (Int): The new value of the local preference (if SetLocalPref is True).
    - SetCommunity (Bool): If `True`, then modify the community.
    - Community (Int): The new value of the community (if SetCommunity is True).
    """

    def __init__(self, ident):
        self.Allow = Bool(f"{ident}.Allow")
        self.MatchAll = Bool(f"{ident}.MatchAll")
        self.Condition = Int(f"{ident}.Condition")
        self.SetLocalPref = Bool(f"{ident}.SetLocalPref")
        self.LocalPref = Int(f"{ident}.LocalPref")
        self.SetCommunity = Bool(f"{ident}.SetCommunity")
        self.Community = Int(f"{ident}.Community")

    def __call__(self, Route):
        """
        Apply the symbolic route map item to the given route.
        """
        Route = copy(Route)

        RouteIsMatched = Or(self.MatchAll, Route.Community == self.Condition)
        RouteIsTransformed = And(Route.Available, RouteIsMatched)

        # The outgoing route is available only if it was available before and it is not dropped.
        # The route is dropped by the item if either the statement is set to Allow, or if the
        # condition does not match.
        Route.Available = And(Route.Available, Or(self.Allow, Not(RouteIsTransformed)))
        Route.LocalPref = If(And(RouteIsTransformed, self.SetLocalPref), self.LocalPref, Route.LocalPref)
        Route.Community = If(And(RouteIsTransformed, self.SetCommunity), self.Community, Route.Community)

        return Route

    def fmt(self):
        """Format the RouteMap nicely. This only works if self has concrete values!"""
        assert self._is_concrete

        actions = []
        if self.Allow:
            if self.SetLocalPref:
                actions.append(f"LocalPref <- {self.LocalPref}")
            if self.SetCommunity:
                actions.append(f"Community <- {self.Community}")
        else:
            actions.append("Drop")

        if self.MatchAll:
            cond = "*"
        else:
            cond = f"Community == {self.Condition}"

        return "{" + cond + " => " + ", ".join(actions) + "}"

    def __str__(self):
        if self.__dict__.get("_is_concrete"):
            return self.fmt()

        s = "{"
        for key, value in self.__dict__.items():
            s += f"\n  {key}: {value},"
        s += "\n}"
        return s

    def __repr__(self):
        return str(self)


@dataclass
class SymbolicRm:
    """
    A symbolic route map is the parametrized form of a route-map. It consists of a (fixed number of)
    symbolic route map items that are applied in series.

    Items (List[SymbolicRmItem]): list of individual symbolic route map items.
    """

    def __init__(self, ident, num):
        self.ident = ident
        self.Items = [SymbolicRmItem(f"{ident}.Item{i}") for i in range(num)]

    def __call__(self, Route):
        """Apply the route map to the given route."""

        for Item in self.Items:
            Route = Item(Route)
        return Route

    def __str__(self):
        items = [str(Item) for Item in self.Items]
        s = f"RouteMap {self.ident}: ["
        for item in items:
            for line in item.splitlines():
                s += f"\n  {line}"
            s += ","
        s += "\n]"
        return s

    def __repr__(self):
        return str(self)
