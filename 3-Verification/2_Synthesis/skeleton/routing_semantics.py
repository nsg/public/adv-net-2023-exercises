from z3 import Or, And, If, Implies
import networkx as nx
from copy import copy

from network import SessionType


def TransformedRoute(net, src, dst):
    """
    Transforms the route that is advertised or propagated from the router `src` to `dst`
    and returns the transformed `BgpRoute`.

    The transformation contains the following steps:
        - Update the `Route.NextHop` and `Route.IgpCost`.
        - Update the `Route.Available` according to the BGP propagation rules.
        - Apply the BGP route-map.

    Each updated `BGPRoute` attribute can have one of the following types:
        - a symbolic integer/boolean variable, or
        - a symbolic expression, e.g., `And(Route.Available, Src.SelectsFromExternal)`, or
        - a concrete value, e.g., `Route.Available = False`.

    - net (Network): The network (topology and configuration)
    - src (str): The name of the source router.
    - dst (str): The name of the destination router.
    """

    # capital Src and Dst refer to the symbolic Router and ExternalRouter structures.
    Src = net.Routers[src]
    Dst = net.Routers[dst]

    # take the route from either the external router or the internal peer and clone it.
    Route = copy(Src.Route)

    # get the session type. If there is no session, then there is also no route to be
    # advertised.
    if (src, dst) not in net.sessions:
        Route.Available = False
        return Route

    session_type = net.sessions[(src, dst)]

    # always update the next-hop field.
    # We can do that because we have an iBGP full-mesh. This should be changed when introducing
    # route-reflection# route-reflWe can do that because we have an iB
    Route.NextHop = Src.id

    # the transformation of the route depends on the session kind
    if session_type == SessionType.EBGP:
        Route.IgpCost = 0
        Route.LocalPref = 100
        # src will advertise the route to dst only if src did not learn it from dst
        if Src.is_internal():
            Route.Available = And(Route.Available, Src.SelectsFrom != Dst.id)

    else:
        # src advertises the route to dst if src has learned the route from an external router.
        # Otherwise, src has learned the route over iBGP, and will therefore not advertise it
        # further over another iBGP session (i.e., we ignore route-maps)
        SrcSelectsFromExternal = Or(
            *(Src.SelectsFrom == R.id for R in net.Routers.values() if R.is_external())
        )
        Route.Available = And(Route.Available, SrcSelectsFromExternal)

        # we can compute the IGP cost as the cost from `dst` to `src`. This only works because
        # we have an iBGP full-mesh, and we always set the NextHop field. As soon as we
        # introduce route-reflection, this computation becomes more complex.
        #
        # We know that Route.NextHop = Src.id, and therefore, we just need to compute the IGP
        # cost from dst to src.Hop is
        Route.IgpCost = nx.shortest_path_length(net.igp, dst, src, weight="weight")

    # finally, apply route maps (if they exist)
    if (src, dst) in net.RouteMaps:
        tf = net.RouteMaps[(src, dst)]
        Route = tf(Route)

    return Route


def RoutingEquations(net):
    """
    Returns equations that fully describe the network routing in the converged state.

    The constraints include:
        - the constraints on the network network routing semantics, and
        - the constraints on the external environment.

    As shown in the lecture, we encode the steady state equation for each BGP session
    `(src, dst)`. In addition, we make sure that a router selects a route if and only if it
    receives at least one route. A difference to the lecture is that we also transform the route
    of `src` with `TransformedRoute(net, src, dst)`. Finally, we encode the allowed external
    environment.

    - net (Network): The network (topology and configuration)
    """
    equations = []

    # add steady-state equations for each session (towards an internal router)
    for src, dst in net.sessions.keys():
        Src = net.Routers[src]
        Dst = net.Routers[dst]

        # only write steady-state equations for internal routers
        if not Dst.is_internal():
            continue

        Route = TransformedRoute(net, src, dst)

        # Steady State Equation
        equations.append(
            If(
                Dst.SelectsFrom == Src.id,
                And(Route.Available, Dst.Route == Route),
                Implies(Route.Available, Dst.Route > Route),
            )
        )

    # Make sure that a router selects a route if and only if it receives at least one route.
    for r, R in net.Routers.items():
        # Only look at internal routers
        if not R.is_internal():
            continue

        RouterSelectsRouteFromNeighbor = Or(
            [
                R.SelectsFrom == net.Routers[src].id
                for src, dst in net.sessions.keys()
                if dst == r
            ]
        )

        RouterReceivesAnyRoute = Or(
            [
                TransformedRoute(net, src, r).Available
                for src, dst in net.sessions.keys()
                if dst == r
            ]
        )

        # What is on the slide 1 to 1
        equations.append(RouterSelectsRouteFromNeighbor == RouterReceivesAnyRoute)
        # You are not allowed to invent a valid route.
        equations.append(RouterSelectsRouteFromNeighbor == R.Route.Available)

    return equations
