"""
Filename: network.py

Description:
    This file defines the `Network`, which describes the topology and the configuration.

    - Network: A class that represents the symbolic network model.
    - Solution: A class that represents a solution to the network model.
    - SessionType: An enumeration describing the different types of BGp sessions.
"""

from z3 import IntVal, BoolVal
import networkx as nx
from copy import copy
from enum import Enum

from routing_state import BgpRoute, Router, ExternalRouter
from route_map import SymbolicRm, SymbolicRmItem


class Network:
    """
    A network stores the network topology and configuration.

    - Routers (dict[str, Router | ExternalRouter]): A lookup table that maps an internal/external
      router name to the router object.
    - routers_rev (dict[int, str]): A lookup table that maps a router id to the router name.
    - sessions (dict[tuple[str, str], SessionKind]): A lookup tables that maps a directed BGP
      session to its session type. The BGP session is represented as a tuple of router names
      `(src, dst)`.
    - RouteMaps (Dict[(str, str), RouteMap]): A lookup table that maps a directed BGP session to
      its route map.
    - igp (nx.Graph): A weighted undirected graph that represents the physical topology with an IGP
      weight on each link.
    """

    def __init__(self):
        """Initializes the network model."""
        self.Routers = {}
        self.routers_rev = {}
        self.sessions = {}
        self.RouteMaps = {}
        self.igp = nx.Graph()

    def pprint(self, solver):
        """
        Pretty-print the network state in the given model.

        - solver (z3.Solver): The solver (that was already checked and returned `sat`)
        """
        print(self.fmt(Solution(solver)))

    def fmt(self, sol):
        """
        Returns a formatted string with the concrete router fields assigned by the solution.

        - sol (Solution): The solution to the network model.
        """
        return (
            "Network {"
            + "\n  Advertised routes: {\n"
            + "\n".join(
                f"    {n}: {R.fmt(self, sol)}"
                for n, R in self.Routers.items()
                if R.is_external()
            )
            + "\n  }"
            + "\n  Selected routes: {\n"
            + "\n".join(
                f"    {n}: {R.fmt(self, sol)}"
                for n, R in self.Routers.items()
                if R.is_internal()
            )
            + "\n  }"
            + "\n}"
        )

    def clone(self, idx):
        """
        Clones the network with new symbolic variables for each router and route.

        All symbolic variables except `self.RouteMaps` will have a new variable name based on
        `idx`.

        - idx (int): The index that decides new symbolic variable names.
        """
        new = Network()
        new.Routers = {n: r.clone(idx) for n, r in self.Routers.items()}
        new.routers_rev = self.routers_rev
        new.sessions = self.sessions
        new.RouteMaps = {
            (src, dst): RouteMap for (src, dst), RouteMap in self.RouteMaps.items()
        }
        new.igp = self.igp
        return new

    def add_router(self, name):
        """
        Adds an internal router to the network with the given name.

        - name (str): The name of the new internal router.
        """
        id = len(self.Routers)
        self.Routers[name] = Router(name, id)
        self.routers_rev[id] = name

    def add_external(self, name):
        """
        Adds an external router to the network with the given name.

        - name (str): The name of the new internal router.
        """
        id = len(self.Routers)
        self.Routers[name] = ExternalRouter(name, id)
        self.routers_rev[id] = name

    def add_link(self, a, b, weight):
        """
        Adds an undirected physical link between `a` and `b` with the given IGP weight.

        - a (str): The name of the first router.
        - b (str): The name of the second router.
        - weight (int): The IGP weight of the link.
        """
        self.igp.add_edge(a, b, weight=weight)

    def add_session(self, a, b, session_type):
        """
        Adds a bidirectional BGP session between `a` and `b` with the given session type.

        - a (str): The name of the first router.
        - b (str): The name of the second router.
        - session_type (SessionType): The session type.
        """
        self.sessions[(a, b)] = session_type
        self.sessions[(b, a)] = session_type

    def set_route_map(self, src, dst, route_map):
        """
        Sets the BGP route map from `src` to `dst`.

        The route-map is applied when `src` advertises or propagates a route to `dst`.

        - src (str): The name of the source router.
        - dst (str): The name of the destination router.
        - route_map (RouteMap): The route-map.
        """
        self.RouteMaps[(src, dst)] = route_map

    def get_environment(self, solver):
        """
        Returns the concrete environments assigned by the solution.

        The environment is a dictionary that maps the name of each external router to a concrete BGP
        route. Each concrete BGP route is a `BgpRoute` for which all variables are replaced by
        constants.

        - solver (z3.Solver): The solver (that was already checked and returned `sat`)
        """
        sol = Solution(solver)
        return {
            ext: sol(Ext.Route)
            for ext, Ext in self.Routers.items()
            if Ext.is_external()
        }

    def get_route_maps(self, solver):
        """
        Returns the concrete route maps assigned by the solution.

        The returned route maps are stored as a dictionary, mapping a pair `(src, dst)` of source
        (`src`) and destination (`dst`) routers (for which there exists a BGP session) to a
        `SymbolicRM` for which all variables are replaced by constants.

        - solver (z3.Solver): The solver (that was already checked and returned `sat`)
        """
        sol = Solution(solver)
        return {
            (src, dst): sol(RouteMap) for (src, dst), RouteMap in self.RouteMaps.items()
        }


class Solution:
    """
    A Solution is a callable function that evaluates a symbolic expression with the concrete values
    assigned by the SMT model stored in the solution.

    - m (z3.ModelRef): The SMT model (of the network).
    """

    def __init__(self, solver):
        """
        Initializes the solution.

        - solver (z3.Solver): The SMT solver that contains the model.
        """
        self.m = solver.model()

    def _get(self, expr):
        """
        Returns the concrete value of the symbolic expression according to the model.

        - expr (z3.ExprRef): The symbolic expression.
        """
        if isinstance(expr, int):
            return IntVal(expr)
        if isinstance(expr, bool):
            return BoolVal(expr)
        if isinstance(expr, str):
            return expr
        return self.m.evaluate(expr, model_completion=True)

    def __call__(self, val):
        """
        Returns the concrete value of `var` assigned by the solution.

        This is a python magic function that is called in `sol(val)`.
        This function handles `BgpRoute` specially.

        - val (z3.ExprRef): The symbolic expression.
        """
        if isinstance(val, Router):
            R = Router(val.name, val.id)
            R.SelectsFrom = self(val.SelectsFrom)
            R.Route = self(val.Route)
            R._is_concrete = True
            return R

        if isinstance(val, ExternalRouter):
            R = Router(val.name, val.id)
            R.Route = self(val.Route)
            R._is_concrete = True
            return R

        if isinstance(val, BgpRoute):
            Route = copy(val)
            for key in Route.__dict__:
                Route.__dict__[key] = self._get(Route.__dict__[key])
            Route._is_concrete = True
            return Route

        if isinstance(val, SymbolicRm):
            RouteMap = SymbolicRm(val.ident, 0)
            RouteMap.Items = [self(Item) for Item in val.Items]
            RouteMap._is_concrete = True
            return RouteMap

        if isinstance(val, SymbolicRmItem):
            Item = copy(val)
            for key in Item.__dict__:
                Item.__dict__[key] = self._get(Item.__dict__[key])
            Item._is_concrete = True
            return Item

        return self._get(val)


class SessionType(Enum):
    """
    An enum that represents different BGP session types.

    - EBGP: An eBGP session between an external router and an internal router.
    - IBGP: An iBGP session between two internal routers.
    """

    EBGP = 0
    IBGP = 1
