"""
Filename: tasks.py

Description:
    This file contains inputs to run Verification(net, c) and Synthesis(net, env) individually.

    - Task_2_1: Inputs to run Verification(net, c) after implementing Task 2.1.
    - Task_2_2: Inputs to run Synthesis(net, env) after implementing Task 2.2.
"""

from z3 import BoolVal, IntVal

from route_map import SymbolicRm
from routing_state import BgpRoute


task_2_1 = {
    ("Cust", "C"): SymbolicRm("CustToC", 1),
    ("Prov", "B"): SymbolicRm("ProvToB", 1),
    ("Peer", "D"): SymbolicRm("PeerToD", 1),
}
task_2_1[("Cust", "C")].Items[0].Allow = BoolVal(True)
task_2_1[("Cust", "C")].Items[0].MatchAll = BoolVal(True)
task_2_1[("Cust", "C")].Items[0].Condition = IntVal(5)
task_2_1[("Cust", "C")].Items[0].SetLocalPref = BoolVal(True)
task_2_1[("Cust", "C")].Items[0].LocalPref = IntVal(101)
task_2_1[("Cust", "C")].Items[0].SetCommunity = BoolVal(False)
task_2_1[("Cust", "C")].Items[0].Community = BoolVal(1)
task_2_1[("Cust", "C")].Items[0]._is_concrete = True
task_2_1[("Cust", "C")]._is_concrete = True

task_2_1[("Prov", "B")].Items[0].Allow = BoolVal(True)
task_2_1[("Prov", "B")].Items[0].MatchAll = BoolVal(False)
task_2_1[("Prov", "B")].Items[0].Condition = IntVal(5)
task_2_1[("Prov", "B")].Items[0].SetLocalPref = BoolVal(True)
task_2_1[("Prov", "B")].Items[0].LocalPref = IntVal(102)
task_2_1[("Prov", "B")].Items[0].SetCommunity = BoolVal(False)
task_2_1[("Prov", "B")].Items[0].Community = BoolVal(1)
task_2_1[("Prov", "B")].Items[0]._is_concrete = True
task_2_1[("Prov", "B")]._is_concrete = True

task_2_1[("Peer", "D")].Items[0].Allow = BoolVal(True)
task_2_1[("Peer", "D")].Items[0].MatchAll = BoolVal(True)
task_2_1[("Peer", "D")].Items[0].Condition = IntVal(5)
task_2_1[("Peer", "D")].Items[0].SetLocalPref = BoolVal(True)
task_2_1[("Peer", "D")].Items[0].LocalPref = IntVal(100)
task_2_1[("Peer", "D")].Items[0].SetCommunity = BoolVal(False)
task_2_1[("Peer", "D")].Items[0].Community = BoolVal(1)
task_2_1[("Peer", "D")].Items[0]._is_concrete = True
task_2_1[("Peer", "D")]._is_concrete = True


_task_2_2_part_1 = {
    "Cust": BgpRoute("Cust"),
    "Prov": BgpRoute("Prov"),
    "Peer": BgpRoute("Peer"),
}

_task_2_2_part_1["Cust"].Available = BoolVal(True)
_task_2_2_part_1["Cust"].LocalPref = IntVal(0)
_task_2_2_part_1["Cust"].AsPathLen = IntVal(1)
_task_2_2_part_1["Cust"].IgpCost = IntVal(0)
_task_2_2_part_1["Cust"].NextHop = IntVal(0)
_task_2_2_part_1["Cust"].Community = IntVal(3)
_task_2_2_part_1["Cust"]._is_concrete = True

_task_2_2_part_1["Prov"].Available = BoolVal(True)
_task_2_2_part_1["Prov"].LocalPref = IntVal(0)
_task_2_2_part_1["Prov"].AsPathLen = IntVal(0)
_task_2_2_part_1["Prov"].IgpCost = IntVal(0)
_task_2_2_part_1["Prov"].NextHop = IntVal(0)
_task_2_2_part_1["Prov"].Community = IntVal(2)
_task_2_2_part_1["Prov"]._is_concrete = True

_task_2_2_part_1["Peer"].Available = BoolVal(True)
_task_2_2_part_1["Peer"].LocalPref = IntVal(0)
_task_2_2_part_1["Peer"].AsPathLen = IntVal(1)
_task_2_2_part_1["Peer"].IgpCost = IntVal(0)
_task_2_2_part_1["Peer"].NextHop = IntVal(0)
_task_2_2_part_1["Peer"].Community = IntVal(1)
_task_2_2_part_1["Peer"]._is_concrete = True

_task_2_2_part_2 = {
    "Cust": BgpRoute("Cust"),
    "Prov": BgpRoute("Prov"),
    "Peer": BgpRoute("Peer"),
}

_task_2_2_part_2["Cust"].Available = BoolVal(False)
_task_2_2_part_2["Cust"].LocalPref = IntVal(0)
_task_2_2_part_2["Cust"].AsPathLen = IntVal(1)
_task_2_2_part_2["Cust"].IgpCost = IntVal(0)
_task_2_2_part_2["Cust"].NextHop = IntVal(0)
_task_2_2_part_2["Cust"].Community = IntVal(0)
_task_2_2_part_2["Cust"]._is_concrete = True

_task_2_2_part_2["Prov"].Available = BoolVal(True)
_task_2_2_part_2["Prov"].LocalPref = IntVal(0)
_task_2_2_part_2["Prov"].AsPathLen = IntVal(0)
_task_2_2_part_2["Prov"].IgpCost = IntVal(0)
_task_2_2_part_2["Prov"].NextHop = IntVal(0)
_task_2_2_part_2["Prov"].Community = IntVal(5)
_task_2_2_part_2["Prov"]._is_concrete = True

_task_2_2_part_2["Peer"].Available = BoolVal(False)
_task_2_2_part_2["Peer"].LocalPref = IntVal(0)
_task_2_2_part_2["Peer"].AsPathLen = IntVal(0)
_task_2_2_part_2["Peer"].IgpCost = IntVal(0)
_task_2_2_part_2["Peer"].NextHop = IntVal(0)
_task_2_2_part_2["Peer"].Community = IntVal(0)
_task_2_2_part_2["Peer"]._is_concrete = True

task_2_2 = [_task_2_2_part_1, _task_2_2_part_2]
