"""
Filename: verification.py

Description:
    This file is the entry point for the exercise.

    We first define a network model and a specification.
    We then verify the specification on the network model and print the result.
    If the specification is violated, we also print a formatted counterexample.

"""

from z3 import Not, And, Or, If, Implies, Solver, sat

from route_map import RouteMap, RmItem
from network import Network, Solution, SessionType
from routing_semantics import RoutingEquations

net = Network()

# add the routers of the network
net.add_external("Customer")
net.add_router("A")
net.add_router("B")
net.add_router("C")
net.add_router("D")
net.add_external("Provider")

# define the network topology
net.add_link("A", "B", 1)
net.add_link("A", "C", 1)
net.add_link("B", "D", 3)
net.add_link("C", "D", 2)
net.add_link("B", "Provider", 1)
net.add_link("C", "Customer", 1)

# network configuration: add the BGP sessions
net.add_session("A", "B", SessionType.IBGP)
net.add_session("A", "C", SessionType.IBGP)
net.add_session("A", "D", SessionType.IBGP)
net.add_session("B", "C", SessionType.IBGP)
net.add_session("B", "D", SessionType.IBGP)
net.add_session("C", "D", SessionType.IBGP)
net.add_session("B", "Provider", SessionType.EBGP)
net.add_session("C", "Customer", SessionType.EBGP)

# Load the specification for this task
from tasks import Task_2_1 as Spec

# Perform the verification
s = Solver()
# build the network model
s.add(RoutingEquations(net))
# add the specification (as an inverse to find counterexamples)
s.add(Not(Spec(net)))

# run the SMT solver
if s.check() == sat:
    # the solver found a way to violate the spec!
    print("Specification is violated!")

    # get the solution
    sol = Solution(s)
    # print the network
    print(net.fmt(sol))

else:
    # the solver did not find a way to violate the spec!
    print("Specification is satisfied!")
