"""
Filename: tasks.py

Description:
    This file contains specifications for each task that should be satisfied after solving the task.

    - Task_1: The specification for Task 1.
    - Task_2_1: The specification for Subtask 2.1.
    - Task_2_2: The specification for Subtask 2.2 (also Subtask 2.4 and 2.5).
    - Task_3: The specification for Subtask 3.2.

"""

from z3 import And, Or, Not, Implies, If


def Task_1(net):
    """
    Specification for task 1:
        "Always prefer the route from the customer."
    """
    Customer = net.Routers["Customer"]
    # Provider = net.Routers["Provider"]
    A = net.Routers["A"]
    B = net.Routers["B"]
    C = net.Routers["C"]
    D = net.Routers["D"]

    return Implies(
        Customer.Route.Available,
        And(
            C.SelectsFrom == Customer.id,
            A.SelectsFrom == C.id,
            B.SelectsFrom == C.id,
            D.SelectsFrom == C.id,
        ),
    )


def Task_2_1(net):
    """
    Specification for the subtask 2.1.

    The specification covers four different cases:

    1. Neither Customer nor Provider advertise a route. --> `SpecNoRoute`
       => In that case, A, B, C, and D select no route.
    2. The network prefers the route from Customer --> `SpecCustomer`
       Either Customer advertises a route and Provider does not, or the Customer's route has a
       shorter AS path length.
       => C selects the route from Customer, and A, B, and D selects the route from C.
    3. The network prefers the route from Provider --> `SpecProvider`
       Either Provider advertises a route and Customer does not, or the Provider's route has a
       shorter AS path length.
       => B selects the route from Provider, and A, C, and D selects the route from B.
    4. Routes from Customer and Provider are equally preferred --> `SpecCustomerProvider`
       Both Customer and Provider advertise a route, and they have the same AS path length.
       => Router B still prefers the route from C because of the lower router ID. Thus, all routers
          select the route from the Customer (or C).

    """
    Customer = net.Routers["Customer"]
    Provider = net.Routers["Provider"]
    A = net.Routers["A"]
    B = net.Routers["B"]
    C = net.Routers["C"]
    D = net.Routers["D"]

    SpecNoRoute = Implies(
        And(Not(Customer.Route.Available), Not(Provider.Route.Available)),
        And(
            Not(A.Route.Available),
            Not(B.Route.Available),
            Not(C.Route.Available),
            Not(D.Route.Available),
        ),
    )
    SpecCustomer = Implies(
        Or(
            And(Customer.Route.Available, Not(Provider.Route.Available)),
            And(
                Customer.Route.Available,
                Provider.Route.Available,
                Customer.Route.AsPathLen < Provider.Route.AsPathLen,
            ),
        ),
        And(
            A.SelectsFrom == C.id,
            B.SelectsFrom == C.id,
            C.SelectsFrom == Customer.id,
            D.SelectsFrom == C.id,
        ),
    )
    SpecProvider = Implies(
        Or(
            And(
                Not(Customer.Route.Available),
                Provider.Route.Available,
            ),
            And(
                Customer.Route.Available,
                Provider.Route.Available,
                Customer.Route.AsPathLen > Provider.Route.AsPathLen,
            ),
        ),
        And(
            A.SelectsFrom == B.id,
            B.SelectsFrom == Provider.id,
            C.SelectsFrom == B.id,
            D.SelectsFrom == B.id,
        ),
    )
    SpecCustomerProvider = Implies(
        And(
            Customer.Route.Available,
            Provider.Route.Available,
            Customer.Route.AsPathLen == Provider.Route.AsPathLen,
        ),
        And(
            A.SelectsFrom == C.id,
            B.SelectsFrom == C.id,
            C.SelectsFrom == Customer.id,
            D.SelectsFrom == C.id,
        ),
    )

    return And(SpecNoRoute, SpecCustomer, SpecProvider, SpecCustomerProvider)


def Task_2_2(net):
    """
    Specification for the subtask 2.2.

    The specification covers four different cases:

    1. Neither Customer nor Provider advertise a route. --> `SpecNoRoute`
       => In that case, A, B, C, and D select no route.
    2. The network prefers the route from Customer --> `SpecCustomer`
       Either Customer advertises a route and Provider does not, or the Customer's route has a
       shorter AS path length.
       => C selects the route from Customer, and A, B, and D selects the route from C.
    3. The network prefers the route from Provider --> `SpecProvider`
       Either Provider advertises a route and Customer does not, or the Provider's route has a
       shorter AS path length.
       => B selects the route from Provider, and A, C, and D selects the route from B.
    4. Routes from Customer and Provider are equally preferred --> `SpecCustomerProvider`
       Both Customer and Provider advertise a route, and they have the same AS path length.
       => C selects the route from Customer and B selects the route from Provider. Due to the
          IGP cost, D selects the route from C. A selects the route from B because of the tie break.
    """
    Customer = net.Routers["Customer"]
    Provider = net.Routers["Provider"]
    A = net.Routers["A"]
    B = net.Routers["B"]
    C = net.Routers["C"]
    D = net.Routers["D"]

    SpecNoRoute = Implies(
        And(Not(Customer.Route.Available), Not(Provider.Route.Available)),
        And(
            Not(A.Route.Available),
            Not(B.Route.Available),
            Not(C.Route.Available),
            Not(D.Route.Available),
        ),
    )
    SpecCustomer = Implies(
        Or(
            And(Customer.Route.Available, Not(Provider.Route.Available)),
            And(
                Customer.Route.Available,
                Provider.Route.Available,
                Customer.Route.AsPathLen < Provider.Route.AsPathLen,
            ),
        ),
        And(
            A.SelectsFrom == C.id,
            B.SelectsFrom == C.id,
            C.SelectsFrom == Customer.id,
            D.SelectsFrom == C.id,
        ),
    )
    SpecProvider = Implies(
        Or(
            And(
                Not(Customer.Route.Available),
                Provider.Route.Available,
            ),
            And(
                Customer.Route.Available,
                Provider.Route.Available,
                Customer.Route.AsPathLen > Provider.Route.AsPathLen,
            ),
        ),
        And(
            A.SelectsFrom == B.id,
            B.SelectsFrom == Provider.id,
            C.SelectsFrom == B.id,
            D.SelectsFrom == B.id,
        ),
    )
    SpecCustomerProvider = Implies(
        And(
            Customer.Route.Available,
            Provider.Route.Available,
            Customer.Route.AsPathLen == Provider.Route.AsPathLen,
        ),
        And(
            A.SelectsFrom == B.id,
            B.SelectsFrom == Provider.id,
            C.SelectsFrom == Customer.id,
            D.SelectsFrom == C.id,
        ),
    )

    return And(SpecNoRoute, SpecCustomer, SpecProvider, SpecCustomerProvider)


def Task_3(net):
    """
    Specification for the subtask 3.2.

    The specification covers two different cases:
    1. Either, the route from `Customer` is available. Then, the entire network must prefer the
       route from Customer  (i.e., B, C, and D select the route from A).
    2. Or, the route from `Customer` is not available. Then, if the route from `Provider` is
       available, the network selects the route from Provider (i.e., A, B, and C select the route
       from D).
    """
    Customer = net.Routers["Customer"]
    Provider = net.Routers["Provider"]
    A = net.Routers["A"]
    B = net.Routers["B"]
    C = net.Routers["C"]
    D = net.Routers["D"]

    return If(
        Customer.Route.Available,
        And(
            A.SelectsFrom == C.id,
            B.SelectsFrom == C.id,
            C.SelectsFrom == Customer.id,
            D.SelectsFrom == C.id,
        ),
        If(
            Provider.Route.Available,
            And(
                A.SelectsFrom == B.id,
                B.SelectsFrom == Provider.id,
                C.SelectsFrom == B.id,
                D.SelectsFrom == B.id,
            ),
            And(
                Not(Provider.Route.Available),
                Not(Customer.Route.Available),
                Not(A.Route.Available),
                Not(B.Route.Available),
                Not(C.Route.Available),
                Not(D.Route.Available),
            ),
        ),
    )
