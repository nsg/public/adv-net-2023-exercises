"""
Filename: route-map.py

Description:
    This file defines classes related to BGP routing states route-maps.

    - RmItem: A class that represents an individual route-map item.
    - RouteMap: A class that represents a route-map.

"""

from z3 import Bool, Int, If, And, Or, Not
from dataclasses import dataclass
from copy import copy


@dataclass
class RmItem:
    """
    A route-map item matches on a community value and modifies the local preference and/or the
    community.

    A route-map item has the following semantics:
    - A route map item can either match or not match on a route depending on `self.condition`.
      If the route *is not* matched, then it remains *unmodified*.
    - If a route *is* matched, then the route is either dropped or modified, depending on
      `self.allow`.
    - If `self.allow = False`, then the route is dropped.
    - If `self.allow = True`, then the route is modified according to `self.local_pref` and
      `self.community`.

    - allow (bool): Whether the item allows or drops matching routes.
    - condition (int | None): The community value that the item matches on.
      - If `condition` is `None`, then the route-map will match every route.
      - Otherwise, the route-map will match a `Route` if `Route.community == self.condition`.
    - local_pref (int | None): The local preference value that the item rewrites when the route
      matches the condition. If it is `None`, then the item does not modify the local preference.
    - community (int | None): The community value that the item rewrites when the route matches the
      condition. If it is `None`, then the item does not modify the community.
    """

    def __init__(self, allow, condition, local_pref, community):
        """Initializes the route-map item."""
        self.allow = allow
        self.condition = condition
        self.local_pref = local_pref
        self.community = community

    def fmt(self, *kargs):
        """Returns a formatted string of the route-map item."""
        actions = []
        if self.allow:
            if self.local_pref is not None:
                actions.append(f"lp <- {self.local_pref}")
            if self.community is not None:
                actions.append(f"c <- {self.community}")
        else:
            actions.append("Drop")

        if not actions:
            return "{}"

        if self.condition is None:
            cond = "*"
        else:
            cond = f"c = {self.condition}"

        return cond + " => " + ", ".join(actions)

    def __call__(self, Route):
        """
        Applies the route-map item to the given route and returns the modified `BgpRoute`.

        The application contains the following steps:
        - Match on the community value.
        - Modify the local preference or/and the community value if the route is matched.
        - update `Route.Available` based on whether a matched route is allowed or dropped.

        If `self.allow = False`, `Route.Available` is set to `False` if the route is matched.
        Each other modified `BgpRoute` attribute is conditionally modified by the symbolic
        expression `If(Cond, NewValue, OldValue)`, where `Cond` is the symbolic expression that is
        evaluated to `True` if the route is available and matched, `NewValue` is the concrete value
        defined in `self.local_pref` or `self.community`, and `OldValue` is the symbolic variable
        of `Route`.

        This is a python magic function that is called in `rm_item(Route)`.

        - Route (BgpRoute): The route to be applied.
        """
        Route = copy(Route)

        # TODO: Task 3

        return Route


@dataclass
class RouteMap:
    """
    A route-map contains a sequence of route-map items that are applied to the route one-by-one.

    A route-map has the following semantics:
    - A route is applied by *all* route-map items in `self.items` in the order they are defined.
    - Whenever a route is matched (and modified) by a route-map item, it still goes through the
      rest of the items and can be matched (and modified) again.
    - If the route is dropped by one item, since the route is no longer available, it will never
      be matched by any other item.
    - If the route is not matched by any item, then it remains the same (i.e., implicitly allowed).

    - items (List[RmItem]): a list of route-map items.
    """

    def __init__(self, items):
        """Initializes the route-map."""
        self.items = items

    def fmt(self, *kargs):
        """Returns a formatted string of the route-map."""
        return "{\n  " + "\n  ".join(item.fmt() for item in self.items) + "\n}"

    def __call__(self, Route):
        """
        Applies the route-map to the given route by applying each item in the route-map.

        - Route (BgpRoute): The route to be applied.
        """

        for item in self.items:
            Route = item(Route)
        return Route
