# Solutions for Task 1: Getting Started

### Subtask 1.1: Understanding the Program Skeleton
First, let's open the file `verification.py`:
- What is the network topology & configuration encoded here?
- Where is the `Spec` for this task loaded from? What does it check?

> The topology, configuration and spec encoded here mimic the example shown in the lecture.
> The spec can be found in `tasks.py` and encodes: "Always prefer the route from the customer."

- Why are both `RoutingEquations(...)` and `Spec(...)` both functions depending on the `net` as an input? What does this allow?

> Modeling `RoutingEquations` and `Spec`s both as functions depending on the `net` as an input allows us to separate the definition
> of a network topology & configuration from the actual symbolic constraints. While the `Spec`s in our examples usually depend on the
> router names, the `RoutingEquations` merely require checking whether two routers are BGP peers, and whether the routers are internal
> or external routers. Therefore, the implementation of `RoutingEquations` will be a universal one, while the `Spec` may need to be
> modified for every network.

Next, open the file `routing_state.py`:
- What BGPRoute attributes from the lecture are implemented already?

> Only the BGP NextHop is encoded already. The rest will follow in Task 2.

- How are the commented-out attributes different from the ones seen in the lecture?

> We only model the ASPathLen, not the actual sequence of AS numbers (for simplicity).

- What differentiates an internal `Router` and an `ExternalRouter`?

> An internal router can learn a route from one of its peers, while an external router can only be assigned
> one directly "out of nowhere" by the SMT solver.

Now, let's open the file `routing_semantics.py`:
- How can you obtain a `Router` (or `ExternalRouter`) object from a `router_name`?

> `net.Routers[router_name]`

- What do the given constraints ensure for a session to an external router?

> The constraints ensure that an internal router only advertises a route to its external peer, if it did not learn this route from the external peer.

- Where in the program skeleton are we missing the equations for route propagation and selection (presented in the lecture)?

> In the function `RoutingEquations(net)`.

Finally, check all the occurrences of the `Available` flag in the program skeleton. What does it encode?

> Depending on the context, the `Available` flag encodes different things:
> - An internal router with `Route.Available == False` means that the router does not select any route.
> - An external router with `Route.Available == False` means that the router does not advertise any route.
> - When transforming a route from router `Src` to router `Dst`, the `Route.Available` encodes whether
>     `Src` can actually propagate its route to `Dst`.

### Subtask 1.2: Implementing the Steady-State Equations

> see `routing-semantics.py` or run `diff -r -u --color skeleton/ solution_1/`
