# Network Verification

In this exercise, you will build a system to verify simple BGP configurations.
It should support basic BGP attributes and functionality (we will ignore advanced features like route reflection).

## Task 1: Getting Started
You are given an unfinished program skeleton that will guide you towards building your very own BGP verifier.
In this task, we will try to understand

### Subtask 1.1: Understanding the Program Skeleton
Let us familiarize with the program skeleton and its structure a bit.
To that end, look at the code/comments and discuss with your peers to answer the following questions.

First, let's open the file `verification.py`:
- What is the network topology & configuration encoded here?
- Where is the `Spec` for this task loaded from? What does it check?
- Why are both `RoutingEquations(...)` and `Spec(...)` both functions depending on the `net` as an input? What does this allow?

Next, open the file `routing_state.py`:
- What BGPRoute attributes from the lecture are implemented already?
- How are the commented-out attributes different from the ones seen in the lecture?
- What differentiates an internal `Router` and an `ExternalRouter`?

Now, let's open the file `routing_semantics.py`:
- How can you obtain a `Router` (or `ExternalRouter`) object from a `router_name`?
- What do the given constraints ensure for a session to an external router?
- Where in the program skeleton are we missing the equations for route propagation and selection (presented in the lecture)?

Finally, check all the occurrences of the `Available` flag in the program skeleton. What does it encode?

### Subtask 1.2: Implementing the Steady-State Equations
Recall the equations introduced in the lecture that ensure correct BGP routing semantics modeling the converged state of the network.
For every session, we have:
```
If rA.SelectsFrom == rB.id
Then Enc[“rB propagates to rA”] ∧ rA.Route == rB.Route
Else Enc[“rB propagates to rA”] ==> rA.Route > rB.Route
```
and
```
Enc[“rA selects from someone”]  <==>  Enc[“rA receives any route”]
```

In this task, we want to include the same logic into the program skeleton!

Once done, we can now run our basic BGP verifier and should find that
```bash
$ python3 verification.py
The specification is satisfied!
```

## Task 2: BGP attributes

Until now, a `BgpRoute` contains the `NextHop` attribute (along with the `Available` flag).
This task aims to add additional attributes to `BgpRoute`.
You will implement (parts of) the [BGP best path selection](https://www.cisco.com/c/en/us/support/docs/ip/border-gateway-protocol-bgp/13753-25.html#toc-hId-232050272).
However, we will not support the complete BGP protocol!
We will consider:
- `LocalPref`
- `AsPathLen` (we will ignore the actual path and just work with the AS path length)
- `IgpCost` (and iBGP vs. eBGP by encoding routes learned from eBGP to have a zero IGP cost)
- `NextHop` (as a tiebreak)

### Subtask 2.1: `AsPathLen`

We will not consider the entire AS path (as SMT is inefficient in handling unbounded lists of numbers), but we will encode the length of the AS path as a single number.
Extend `BgpRoute` to contain the `AsPathLen` attribute.
For that, you will need to extend four functions of `BgpRoute`:
- `__init__`: Create a symbolic variable `AsPathLen` to represent the AS path length as a symbolic integer (`Int`).
- `__eq__`: Extend the equality operation to check for the `AsPathLen`.  
  *Hint*: This function must return an expression that is `True` only if `self` has the same values in all attributes as `other`.
- `__gt__`: Extend the comparison operator between two routes.
  The route with the shorter `AsPathLen` should preferred over another. 
  If the `AsPathLen` of two routes are equal, then break the tie using `NextHop`.  
  *Hint*: You should return an expression that is `True` only if `self` must be preferred over `other`.
- `fmt`: Extend the formatting function to print the value of `r.AsPathLen`:
  ```python
  f"AS path len: {r.AsPathLen}, "
  ```

*Validate your solution*:
Import the `Task_2_1` as the `Spec` in `verification.py`:
```python
from tasks import Task_2_1 as Spec
```
After correctly implementing `AsPathLen`, running `python3 verification.py` will confirm that the network satisfies the specification.

### Subtask 2.2: `IgpCost`

The next aspect of BGP we implement is the `IgpCost`.
Even though the `IgpCost` is technically not a BGP attribute, we still treat it as such (that allows us to compare two routes only by using the `A > B` operator.)
Similar to Subtask 2.1, you need to extend the four functions of the `BgpRoute`: `__init__`, `__eq__`, `__gt__`, and `fmt`.

Next, extend the function `TransformedRoute` in the file `routing_semantics.py` to compute the IGP cost.
When router `src` advertises `Route` to `dst` over an iBGP session, then the `Route.IgpCost` should be set to the shortest-path cost from `dst` to `src` within the network.
If `src` advertises `Route` to `dst` over an eBGP session, then `Route.IgpCost` must equal zero.
The `Network` maintains a graph `igp` on which you can compute the shortest path from `src` to `dst`:
```python
nx.shortest_path_length(net.igp, src, dst, weight="weight")
```

*Hint*: The function `TransformedRoute` does not create any equations.
Instead, it returns a new `BgpRoute` object where attributes like the `IgpCost` can be overwritten to have a concrete value rather than a symbolic one.
For instance, to set the `IgpCost` of a route to 0, you write `Route.IgpCost = 0`.

*Validate your solution*:
Import the `Task_2_2` as the `Spec` in `verification.py`:
```python
from tasks import Task_2_2 as Spec
```
Before implementing `IgpCost`, running `python3 verification.py` should yield a counterexample, but after correctly implementing `IgpCost`, the specification should be satisfied.

### Question 2.3

How could we support route reflection (i.e., the possibility that routes can propagate over multiple sessions within iBGP)?
How would you assign `NextHop` and `IgpCost` in the TransformedRoute function? (A brief explanation is sufficient.)

### Subtask 2.4: `LocalPref`

Extend `BgpRoute` to also contain the `LocalPref` attribute.
To that end, you need to modify the four functions of `BgpRoute`: `__init__`, `__eq__`, `__gt__`, and `fmt`. 
  
Remember that the `LocalPref` is a non-transitive BGP attribute, i.e., the `LocalPref` is never transmitted over an eBGP session.
In our simple model, a router must reset `LocalPref` to its default value of 100 whenever it receives a route over eBGP.
Modify the function `TransformedRoute` to set the `LocalPref` attribute to 100 if the session from `src` to `dst` is a `SessionType.EBGP`.

Notice that we only reset the `LocalPref` to 100, meaning that every internal router can only see a `LocalPref` of 100. 
We will change that in Task 3 when you implement route maps.

*Validate your solution*:
Run `python3 verification.py` using the specification from Subtask 2.2 and ensure that the model is still satisfied.

### Subtask 2.5: `Communities`

Any BGP route can have attached a set of communities.
Similar to the AS path, we do not model a set of possible communities.
Instead, we assume that each route always has one community assigned (which we encode as an integer).
Extend `BgpRoute` to also contain the `Community` attribute.
To that end, you need to modify the three functions of `BgpRoute`: `__init__`, `__eq__`, and `fmt` (you can leave `__gt__` unchanged since the community does not influence the decision process).

*Validate your solution*:
Run `python3 verification.py` using the specification from Subtask 2.2 and ensure that the model is still satisfied.
