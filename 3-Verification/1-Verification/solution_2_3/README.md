# Solutions for Task 2.3

How could we support route reflection (i.e., the possibility that routes can propagate over multiple sessions within iBGP)?
Briefly describe how you would assign `NextHop` and `IgpCost` in the function `TransformedRoute`.

> Let's consider internal router `src` that advertises a route to `dst` over iBGP.
> With route reflection, `src` does not update the BGP `NextHop` attribute if `src` is a route reflector and has learned the route from another internal router.
> Thus, the `NextHop` that `dst` receives might not be `src`.
> The `IgpCost` must equal not equal the shortest path cost from `dst` to `src`, but to `NextHop`.
> Consequently, we need to *symbolically* compute the IGP cost from `dst` to `NextHop`.
>
> *The following is not part of your assignment!*
> To *symbolically* compute the `IgpCost`, we need to compute the shortest path cost from `dst` to every other internal router and use a symbolic "*switch-case*" expression as follows:
> 
> ```python
> Route.IgpCost = If(
>     Route.NextHop == A.id,
>     nx.shortest_path_length(net.igp, dst, "A", weight="weight"),
>     If(
>         Route.NextHop == B.id,
>         nx.shortest_path_length(net.igp, dst, "B", weight="weight"),
>         If(
>             Route.NextHop == C.id,
>             nx.shortest_path_length(net.igp, dst, "C", weight="weight"),
>             1000000
>         )
>     )
> )
> ```
>
> To implement such an expression *generally* in the function `TransformedRoute`, we need to build it up in a loop:
>
> ```python
> Cost = 1000000  # A big number
> # iterate over all routers
> for r, R in net.Routers.items():
>     # compute the igp cost to that router.
>     igp_cost = nx.shortest_path_length(net.igp, dst, r, weight="weight")
>     # Generate the IF expression. If the NextHop is set to R, then the IGP cost is equal to the
>     # igp_cost computed above.
>     Cost = If(Route.NextHop == R.id, igp_cost, Cost)
> Route.IgpCost = Cost
> ```
