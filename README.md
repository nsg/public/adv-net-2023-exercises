# Advanced Topics in Communication Networks 2023 Course Repository 📡

Hello there! Welcome aboard. 🚀

This repository houses all the juicy stuff you'll need for the course. You'll find weekly exercises neatly organized into various blocks. And don't you worry about the technical mumbo jumbo; if an exercise requires a specific virtual environment, all the step-by-step instructions are right here!

## Table of Contents
- [Block 1: Routing](./1-Routing/)
- [Block 2: Programmability](./2-Programmability/)
- [Block 3: Verification](./3-Verification/)
- [Block 4: Measurements](./4-Measurements/)
- [Block 5: Security](./5-Security/)
- [Block 6: Transport](./6-Transport/)
- [Block 7: Sustainability](./7-Sustainability/)

Enjoy the learning journey! 📘
