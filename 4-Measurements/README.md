# Probabilistic Data Structures

## Overview
This week, we are focusing on two engaging exercises: "Heavy Hitter Detector" and "Count-Min Sketch". These exercises are designed to deepen your understanding of probabilistic data structures and their implementation in network systems.

## Exercise 1: Heavy Hitter Detector
- **Objective**: Implement and test a Bloom Filter using P4.
- **Key Concepts**: 
  - Understanding the mechanics of Bloom Filters.
  - Learning to implement these filters in P4.
- **Tasks**: 
  - Implement a Bloom Filter for packet processing.
  - Test the filter's functionality in various scenarios.

## Exercise 2: Count-Min Sketch
- **Objective**: Implement a Count-Min Sketch with P4 and a controller for decoding the counters.
- **Key Concepts**: 
  - Grasping the principles of the Count-Min Sketch algorithm.
  - Utilizing P4 and controller interactions.
- **Tasks**:
  - Develop a Count-Min Sketch for packet flow analysis.
  - Implement a controller for decoding and analyzing the sketch data.
  - Experiment with sketch dimensions and observe outcomes.

#### Common Themes
Both exercises emphasize:
- The practical implementation of theoretical concepts in network systems.
- The use of P4 programming for network data processing.
- The significance of probabilistic data structures in efficiently managing network traffic.

#### Preparation
- Review the provided READMEs for detailed instructions and setup requirements.
- Familiarize yourself with the P4 programming environment and the specific tools and libraries used in these exercises.
